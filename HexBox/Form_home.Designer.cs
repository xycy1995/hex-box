﻿namespace HexBox
{
    partial class Form_home
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_home));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.视图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工具列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.展开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.折叠ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工具窗体ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.全部关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.全部正常化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.全部最小化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.全部最大化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工具布局ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.层叠ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.平铺ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.水平平铺ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.垂直平铺ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.排列图标ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.窗口置顶ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设置置顶ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.取消置顶ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.选项ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.插件管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.使用指南ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.软件更新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于软件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel_left = new System.Windows.Forms.Panel();
            this.panel_left_tools_search = new System.Windows.Forms.Panel();
            this.button_search = new System.Windows.Forms.Button();
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.panel_left_tools = new System.Windows.Forms.Panel();
            this.listView_tools = new System.Windows.Forms.ListView();
            this.button_fold = new System.Windows.Forms.Button();
            this.panel_left_sign = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.panel_left.SuspendLayout();
            this.panel_left_tools_search.SuspendLayout();
            this.panel_left_tools.SuspendLayout();
            this.panel_left_sign.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.视图ToolStripMenuItem,
            this.选项ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(726, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.退出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Image = global::HexBox.Properties.Resources.exit_x48;
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 视图ToolStripMenuItem
            // 
            this.视图ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.工具列表ToolStripMenuItem,
            this.工具窗体ToolStripMenuItem,
            this.工具布局ToolStripMenuItem,
            this.窗口置顶ToolStripMenuItem});
            this.视图ToolStripMenuItem.Name = "视图ToolStripMenuItem";
            this.视图ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.视图ToolStripMenuItem.Text = "视图";
            // 
            // 工具列表ToolStripMenuItem
            // 
            this.工具列表ToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.工具列表ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.展开ToolStripMenuItem,
            this.折叠ToolStripMenuItem});
            this.工具列表ToolStripMenuItem.Image = global::HexBox.Properties.Resources.list_x48;
            this.工具列表ToolStripMenuItem.Name = "工具列表ToolStripMenuItem";
            this.工具列表ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.工具列表ToolStripMenuItem.Text = "工具列表";
            // 
            // 展开ToolStripMenuItem
            // 
            this.展开ToolStripMenuItem.Image = global::HexBox.Properties.Resources.check_x48;
            this.展开ToolStripMenuItem.Name = "展开ToolStripMenuItem";
            this.展开ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.O)));
            this.展开ToolStripMenuItem.Size = new System.Drawing.Size(175, 26);
            this.展开ToolStripMenuItem.Text = "展开";
            this.展开ToolStripMenuItem.Click += new System.EventHandler(this.展开ToolStripMenuItem_Click);
            // 
            // 折叠ToolStripMenuItem
            // 
            this.折叠ToolStripMenuItem.Name = "折叠ToolStripMenuItem";
            this.折叠ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.折叠ToolStripMenuItem.Size = new System.Drawing.Size(175, 26);
            this.折叠ToolStripMenuItem.Text = "折叠";
            this.折叠ToolStripMenuItem.Click += new System.EventHandler(this.折叠ToolStripMenuItem_Click);
            // 
            // 工具窗体ToolStripMenuItem
            // 
            this.工具窗体ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.全部关闭ToolStripMenuItem,
            this.全部正常化ToolStripMenuItem,
            this.全部最小化ToolStripMenuItem,
            this.全部最大化ToolStripMenuItem});
            this.工具窗体ToolStripMenuItem.Image = global::HexBox.Properties.Resources.window_x48;
            this.工具窗体ToolStripMenuItem.Name = "工具窗体ToolStripMenuItem";
            this.工具窗体ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.工具窗体ToolStripMenuItem.Text = "工具窗体";
            // 
            // 全部关闭ToolStripMenuItem
            // 
            this.全部关闭ToolStripMenuItem.Name = "全部关闭ToolStripMenuItem";
            this.全部关闭ToolStripMenuItem.Size = new System.Drawing.Size(167, 26);
            this.全部关闭ToolStripMenuItem.Text = "全部关闭";
            this.全部关闭ToolStripMenuItem.Click += new System.EventHandler(this.全部关闭ToolStripMenuItem_Click);
            // 
            // 全部正常化ToolStripMenuItem
            // 
            this.全部正常化ToolStripMenuItem.Name = "全部正常化ToolStripMenuItem";
            this.全部正常化ToolStripMenuItem.Size = new System.Drawing.Size(167, 26);
            this.全部正常化ToolStripMenuItem.Text = "全部正常化";
            this.全部正常化ToolStripMenuItem.Click += new System.EventHandler(this.全部正常化ToolStripMenuItem_Click);
            // 
            // 全部最小化ToolStripMenuItem
            // 
            this.全部最小化ToolStripMenuItem.Name = "全部最小化ToolStripMenuItem";
            this.全部最小化ToolStripMenuItem.Size = new System.Drawing.Size(167, 26);
            this.全部最小化ToolStripMenuItem.Text = "全部最小化";
            this.全部最小化ToolStripMenuItem.Click += new System.EventHandler(this.全部最小化ToolStripMenuItem_Click);
            // 
            // 全部最大化ToolStripMenuItem
            // 
            this.全部最大化ToolStripMenuItem.Name = "全部最大化ToolStripMenuItem";
            this.全部最大化ToolStripMenuItem.Size = new System.Drawing.Size(167, 26);
            this.全部最大化ToolStripMenuItem.Text = "全部最大化";
            this.全部最大化ToolStripMenuItem.Click += new System.EventHandler(this.全部最大化ToolStripMenuItem_Click);
            // 
            // 工具布局ToolStripMenuItem
            // 
            this.工具布局ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.层叠ToolStripMenuItem,
            this.平铺ToolStripMenuItem,
            this.排列图标ToolStripMenuItem});
            this.工具布局ToolStripMenuItem.Image = global::HexBox.Properties.Resources.layout_x48;
            this.工具布局ToolStripMenuItem.Name = "工具布局ToolStripMenuItem";
            this.工具布局ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.工具布局ToolStripMenuItem.Text = "工具布局";
            // 
            // 层叠ToolStripMenuItem
            // 
            this.层叠ToolStripMenuItem.Name = "层叠ToolStripMenuItem";
            this.层叠ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.层叠ToolStripMenuItem.Text = "层叠";
            this.层叠ToolStripMenuItem.Click += new System.EventHandler(this.层叠ToolStripMenuItem_Click);
            // 
            // 平铺ToolStripMenuItem
            // 
            this.平铺ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.水平平铺ToolStripMenuItem,
            this.垂直平铺ToolStripMenuItem});
            this.平铺ToolStripMenuItem.Name = "平铺ToolStripMenuItem";
            this.平铺ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.平铺ToolStripMenuItem.Text = "平铺";
            // 
            // 水平平铺ToolStripMenuItem
            // 
            this.水平平铺ToolStripMenuItem.Name = "水平平铺ToolStripMenuItem";
            this.水平平铺ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.水平平铺ToolStripMenuItem.Text = "水平平铺";
            this.水平平铺ToolStripMenuItem.Click += new System.EventHandler(this.水平平铺ToolStripMenuItem_Click);
            // 
            // 垂直平铺ToolStripMenuItem
            // 
            this.垂直平铺ToolStripMenuItem.Name = "垂直平铺ToolStripMenuItem";
            this.垂直平铺ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.垂直平铺ToolStripMenuItem.Text = "垂直平铺";
            this.垂直平铺ToolStripMenuItem.Click += new System.EventHandler(this.垂直平铺ToolStripMenuItem_Click);
            // 
            // 排列图标ToolStripMenuItem
            // 
            this.排列图标ToolStripMenuItem.Name = "排列图标ToolStripMenuItem";
            this.排列图标ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.排列图标ToolStripMenuItem.Text = "排列图标";
            this.排列图标ToolStripMenuItem.Click += new System.EventHandler(this.排列图标ToolStripMenuItem_Click);
            // 
            // 窗口置顶ToolStripMenuItem
            // 
            this.窗口置顶ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置置顶ToolStripMenuItem,
            this.取消置顶ToolStripMenuItem});
            this.窗口置顶ToolStripMenuItem.Image = global::HexBox.Properties.Resources.topmost_x48;
            this.窗口置顶ToolStripMenuItem.Name = "窗口置顶ToolStripMenuItem";
            this.窗口置顶ToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.窗口置顶ToolStripMenuItem.Text = "窗口置顶";
            // 
            // 设置置顶ToolStripMenuItem
            // 
            this.设置置顶ToolStripMenuItem.Name = "设置置顶ToolStripMenuItem";
            this.设置置顶ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Up)));
            this.设置置顶ToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.设置置顶ToolStripMenuItem.Text = "设置置顶";
            this.设置置顶ToolStripMenuItem.Click += new System.EventHandler(this.设置置顶ToolStripMenuItem_Click);
            // 
            // 取消置顶ToolStripMenuItem
            // 
            this.取消置顶ToolStripMenuItem.Image = global::HexBox.Properties.Resources.check_x48;
            this.取消置顶ToolStripMenuItem.Name = "取消置顶ToolStripMenuItem";
            this.取消置顶ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Down)));
            this.取消置顶ToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.取消置顶ToolStripMenuItem.Text = "取消置顶";
            this.取消置顶ToolStripMenuItem.Click += new System.EventHandler(this.取消置顶ToolStripMenuItem_Click);
            // 
            // 选项ToolStripMenuItem
            // 
            this.选项ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置ToolStripMenuItem,
            this.插件管理ToolStripMenuItem});
            this.选项ToolStripMenuItem.Name = "选项ToolStripMenuItem";
            this.选项ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.选项ToolStripMenuItem.Text = "选项";
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.Image = global::HexBox.Properties.Resources.settings_x48;
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.设置ToolStripMenuItem.Text = "设置";
            this.设置ToolStripMenuItem.Click += new System.EventHandler(this.设置ToolStripMenuItem_Click);
            // 
            // 插件管理ToolStripMenuItem
            // 
            this.插件管理ToolStripMenuItem.Image = global::HexBox.Properties.Resources.plugin_x48;
            this.插件管理ToolStripMenuItem.Name = "插件管理ToolStripMenuItem";
            this.插件管理ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.P)));
            this.插件管理ToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.插件管理ToolStripMenuItem.Text = "插件管理";
            this.插件管理ToolStripMenuItem.Click += new System.EventHandler(this.插件管理ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.使用指南ToolStripMenuItem,
            this.软件更新ToolStripMenuItem,
            this.关于软件ToolStripMenuItem});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 使用指南ToolStripMenuItem
            // 
            this.使用指南ToolStripMenuItem.Image = global::HexBox.Properties.Resources.book_x48;
            this.使用指南ToolStripMenuItem.Name = "使用指南ToolStripMenuItem";
            this.使用指南ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.使用指南ToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.使用指南ToolStripMenuItem.Text = "使用指南";
            this.使用指南ToolStripMenuItem.Click += new System.EventHandler(this.使用指南ToolStripMenuItem_Click);
            // 
            // 软件更新ToolStripMenuItem
            // 
            this.软件更新ToolStripMenuItem.Image = global::HexBox.Properties.Resources.update_x48;
            this.软件更新ToolStripMenuItem.Name = "软件更新ToolStripMenuItem";
            this.软件更新ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.U)));
            this.软件更新ToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.软件更新ToolStripMenuItem.Text = "软件更新";
            this.软件更新ToolStripMenuItem.Click += new System.EventHandler(this.软件更新ToolStripMenuItem_Click);
            // 
            // 关于软件ToolStripMenuItem
            // 
            this.关于软件ToolStripMenuItem.Image = global::HexBox.Properties.Resources.about_x48;
            this.关于软件ToolStripMenuItem.Name = "关于软件ToolStripMenuItem";
            this.关于软件ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.关于软件ToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.关于软件ToolStripMenuItem.Text = "关于软件";
            this.关于软件ToolStripMenuItem.Click += new System.EventHandler(this.关于软件ToolStripMenuItem_Click);
            // 
            // panel_left
            // 
            this.panel_left.Controls.Add(this.panel_left_tools_search);
            this.panel_left.Controls.Add(this.panel_left_tools);
            this.panel_left.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_left.Location = new System.Drawing.Point(0, 28);
            this.panel_left.Name = "panel_left";
            this.panel_left.Size = new System.Drawing.Size(207, 477);
            this.panel_left.TabIndex = 4;
            // 
            // panel_left_tools_search
            // 
            this.panel_left_tools_search.BackColor = System.Drawing.SystemColors.Control;
            this.panel_left_tools_search.Controls.Add(this.button_search);
            this.panel_left_tools_search.Controls.Add(this.textBox_search);
            this.panel_left_tools_search.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_left_tools_search.Location = new System.Drawing.Point(0, 0);
            this.panel_left_tools_search.Name = "panel_left_tools_search";
            this.panel_left_tools_search.Size = new System.Drawing.Size(207, 39);
            this.panel_left_tools_search.TabIndex = 0;
            // 
            // button_search
            // 
            this.button_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_search.Location = new System.Drawing.Point(150, 5);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(54, 29);
            this.button_search.TabIndex = 1;
            this.button_search.Text = "搜索";
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // textBox_search
            // 
            this.textBox_search.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_search.Location = new System.Drawing.Point(3, 6);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(141, 27);
            this.textBox_search.TabIndex = 0;
            // 
            // panel_left_tools
            // 
            this.panel_left_tools.BackColor = System.Drawing.SystemColors.Control;
            this.panel_left_tools.Controls.Add(this.listView_tools);
            this.panel_left_tools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_left_tools.Location = new System.Drawing.Point(0, 0);
            this.panel_left_tools.Name = "panel_left_tools";
            this.panel_left_tools.Size = new System.Drawing.Size(207, 477);
            this.panel_left_tools.TabIndex = 0;
            // 
            // listView_tools
            // 
            this.listView_tools.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView_tools.LabelWrap = false;
            this.listView_tools.Location = new System.Drawing.Point(0, 39);
            this.listView_tools.MultiSelect = false;
            this.listView_tools.Name = "listView_tools";
            this.listView_tools.Size = new System.Drawing.Size(207, 438);
            this.listView_tools.TabIndex = 0;
            this.listView_tools.UseCompatibleStateImageBehavior = false;
            this.listView_tools.View = System.Windows.Forms.View.Tile;
            this.listView_tools.DoubleClick += new System.EventHandler(this.listView_tools_DoubleClick);
            // 
            // button_fold
            // 
            this.button_fold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_fold.Image = global::HexBox.Properties.Resources.arrow_left_x16;
            this.button_fold.Location = new System.Drawing.Point(0, 0);
            this.button_fold.Name = "button_fold";
            this.button_fold.Size = new System.Drawing.Size(16, 477);
            this.button_fold.TabIndex = 0;
            this.button_fold.UseVisualStyleBackColor = true;
            this.button_fold.Click += new System.EventHandler(this.button_fold_Click);
            // 
            // panel_left_sign
            // 
            this.panel_left_sign.Controls.Add(this.button_fold);
            this.panel_left_sign.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_left_sign.Location = new System.Drawing.Point(207, 28);
            this.panel_left_sign.Name = "panel_left_sign";
            this.panel_left_sign.Size = new System.Drawing.Size(16, 477);
            this.panel_left_sign.TabIndex = 6;
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 505);
            this.Controls.Add(this.panel_left_sign);
            this.Controls.Add(this.panel_left);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(387, 62);
            this.Name = "Form_home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "六角魔盒";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_home_FormClosing);
            this.Load += new System.EventHandler(this.Form_home_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel_left.ResumeLayout(false);
            this.panel_left_tools_search.ResumeLayout(false);
            this.panel_left_tools_search.PerformLayout();
            this.panel_left_tools.ResumeLayout(false);
            this.panel_left_sign.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem 文件ToolStripMenuItem;
        private ToolStripMenuItem 退出ToolStripMenuItem;
        private ToolStripMenuItem 视图ToolStripMenuItem;
        private ToolStripMenuItem 选项ToolStripMenuItem;
        private ToolStripMenuItem 设置ToolStripMenuItem;
        private ToolStripMenuItem 帮助ToolStripMenuItem;
        private ToolStripMenuItem 使用指南ToolStripMenuItem;
        private ToolStripMenuItem 关于软件ToolStripMenuItem;
        private ToolStripMenuItem 工具列表ToolStripMenuItem;
        private ToolStripMenuItem 软件更新ToolStripMenuItem;
        private ToolStripMenuItem 工具布局ToolStripMenuItem;
        private ToolStripMenuItem 层叠ToolStripMenuItem;
        private ToolStripMenuItem 平铺ToolStripMenuItem;
        private ToolStripMenuItem 水平平铺ToolStripMenuItem;
        private ToolStripMenuItem 垂直平铺ToolStripMenuItem;
        private ListView listView_tools;
        private Panel panel_left_tools;
        private Panel panel_left_tools_search;
        private Button button_search;
        private TextBox textBox_search;
        private Panel panel_left_sign;
        private ToolStripMenuItem 窗口置顶ToolStripMenuItem;
        private ToolStripMenuItem 排列图标ToolStripMenuItem;
        private ToolStripMenuItem 工具窗体ToolStripMenuItem;
        private ToolStripMenuItem 全部关闭ToolStripMenuItem;
        private ToolStripMenuItem 全部最小化ToolStripMenuItem;
        private ToolStripMenuItem 全部最大化ToolStripMenuItem;
        private ToolStripMenuItem 全部正常化ToolStripMenuItem;
        private ToolStripMenuItem 插件管理ToolStripMenuItem;
        internal Button button_fold;
        internal ToolStripMenuItem 展开ToolStripMenuItem;
        internal ToolStripMenuItem 折叠ToolStripMenuItem;
        internal Panel panel_left;
        internal ToolStripMenuItem 设置置顶ToolStripMenuItem;
        internal ToolStripMenuItem 取消置顶ToolStripMenuItem;
    }
}