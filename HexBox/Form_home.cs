using System.Diagnostics;
using HexBox.Functions.Other;

namespace HexBox
{
    public partial class Form_home : Form
    {
        // 实例一个接口插件类
        Functions.Plugin.CPluginOperate plugin = new();


        /// <summary>
        /// 窗口构造函数
        /// </summary>
        public Form_home()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 当用户加载窗体时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_home_Load(object sender, EventArgs e)
        {
            // 1. 初始化主界面
            CApp.Init(this); 

            // 2. 加载插件并显示到工具列表
            // 加载当前路径下的 plugins 目录
            plugin.LoadPlugins(Properties.Settings.Default.PluginsDirPath);

            // 新建一个图片列表，用来后面给列表项目提供图标
            ImageList imageList = new();         // 实例化图片列表

            // 把插件显示到左侧工具栏
            int index = 0;  // 用于记录索引

            // 遍历插件列表，将每个插件显示到工具列表上
            foreach (var tool in plugin.PluginList)
            {
                // 添加列表项目的文字
                listView_tools.Items.Add(tool.Name);

                // 添加列表项目的图标
                imageList.Images.Add(tool.Icon);                // 将插件的图标添加到 ImageList 里
                listView_tools.LargeImageList = imageList;      // 设置大图标
                listView_tools.SmallImageList = imageList;      // 设置小图标
                listView_tools.Items[index].ImageIndex = index; // 设置对应的图片列表索引号

                index++;                                        // 索引+1
            }


        }

        /// <summary>
        /// 点击展开工具列表按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 展开ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CApp.OpenToolList(this);
        }

        /// <summary>
        /// 点击折叠工具列表按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 折叠ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CApp.CloseToolList(this);
        }


        /// <summary>
        /// 点击折叠按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_fold_Click(object sender, EventArgs e)
        {
            // 如果侧边栏处于打开状态
            if (panel_left.Visible == true)
            {
                CApp.CloseToolList(this);

            }
            // 如果侧边栏处于关闭状态
            else
            {
                CApp.OpenToolList(this);
            }

        }


        /// <summary>
        /// 点击退出应用按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 根据设置文件选择退出应用时是否弹出确认对话框
            if (Properties.Settings.Default.ExitAsk)
            {
                if (MessageBox.Show("是否退出应用？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    CApp.Exit();
                }
            }
            else
            {
                CApp.Exit();
            }
        }

        /// <summary>
        /// 点击窗口层叠按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 层叠ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }


        /// <summary>
        /// 点击窗口水平平铺按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 水平平铺ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        /// <summary>
        /// 点击窗口垂直平铺按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 垂直平铺ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        /// <summary>
        /// 点击窗口排列图标按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 排列图标ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 工具最小化时有效
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        /// <summary>
        /// 点击窗口全部关闭按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 全部关闭ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form midChildren in MdiChildren)
            {
                midChildren.Close();
            }

        }

        /// <summary>
        /// 点击窗口全部正常化按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 全部正常化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form midChildren in MdiChildren)
            {
                midChildren.WindowState = FormWindowState.Normal;
            }
        }

        /// <summary>
        /// 点击窗口全部最小化按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 全部最小化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form midChildren in MdiChildren)
            {
                midChildren.WindowState = FormWindowState.Minimized;
            }
        }

        /// <summary>
        /// 点击窗口全部最大化按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 全部最大化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form midChildren in MdiChildren)
            {
                midChildren.WindowState = FormWindowState.Maximized;
            }
        }

        /// <summary>
        /// 点击软件更新按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 软件更新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // 启动默认浏览器
                Process.Start("explorer.exe", Properties.Settings.Default.UpdateUrl);
            }
            catch
            {
                // 复制地址到剪切板
                Clipboard.SetText(Properties.Settings.Default.UpdateUrl);

                MessageBox.Show("打开浏览器失败，已经复制地址到剪贴板，请手动操作。", "注意", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// 点击使用指南按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 使用指南ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // 启动默认浏览器
                Process.Start("explorer.exe", Properties.Settings.Default.HelpUrl);
            }
            catch
            {
                // 复制地址到剪切板
                Clipboard.SetText(Properties.Settings.Default.AuthorSpace);

                MessageBox.Show("打开浏览器失败，已经复制地址到剪贴板，请手动操作。", "注意", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// 点击关于软件按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 关于软件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubForms.Form_about form_About = new();
            form_About.ShowDialog();
        }


        /// <summary>
        /// 双击工具栏的项目时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView_tools_DoubleClick(object sender, EventArgs e)
        {
            // 判断是否选中某个项目（而不是点击在了空白处）
            if (listView_tools.SelectedItems.Count > 0)
            {
                // 获取被点击项目的索引号
                int index = listView_tools.SelectedItems[0].Index;

                // DEBUG 对应插件的信息
                Debug.WriteLine($"【Form_home】{plugin.PluginList[index].Name}({plugin.PluginList[index].Version}): {plugin.PluginList[index].Describe}");

                // 显示工具窗口
                Form form = (Form)plugin.PluginList[index].CreateFormInstance();    // 获得创建的窗口实例
                form.MdiParent = this;                                              // 绑定为主窗口的 MDI 子窗口
                form.Show();                                                        // 显示窗口
            }
        }


        /// <summary>
        /// 点击搜索工具列表按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_search_Click(object sender, EventArgs e)
        {
            string search_key = textBox_search.Text.Trim(); // 取得去除首尾空格后的搜索关键词
            string itemText;                                // 用于存放工具名

            if (search_key != "")   // 前提是有内容
            {
                foreach (ListViewItem tool in listView_tools.Items)
                {
                    itemText = tool.Text;   // 获取工具名

                    // 如果匹配到关键词
                    if (itemText.Contains(search_key))
                    {
                        Debug.WriteLine("匹配到：" + itemText);
                        listView_tools.FindItemWithText(itemText).Selected = true;  // 选中符合的项目
                        listView_tools.SelectedItems[0].EnsureVisible();            // 跳到选中项的位置
                        break;                                                      // 如果找到了就跳出遍历
                    }

                }
            }
        }

        /// <summary>
        /// 点击设置按钮时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubForms.Form_settings form_Settings = new();
            form_Settings.ShowDialog();
        }

        /// <summary>
        /// 点击设置置顶时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 设置置顶ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CApp.SetTopMost(this);
        }

        /// <summary>
        /// 点击取消置顶时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 取消置顶ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CApp.CancelTopMost(this);
        }

        /// <summary>
        /// 点击插件管理时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 插件管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubForms.Form_pluginManage form_pluginManage = new();
            form_pluginManage.ShowDialog();
        }


        /// <summary>
        /// 拦截窗口关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_home_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 取消关闭
            e.Cancel = true;

            // 根据设置文件选择退出应用时是否弹出确认对话框
            if (Properties.Settings.Default.ExitAsk)
            {
                if (MessageBox.Show("是否退出应用？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    CApp.Exit();
                }
            }
            else
            {
                CApp.Exit();
            }

        }
    }
}