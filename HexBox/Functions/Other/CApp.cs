﻿/*****************************************
 * 应用整体的控制
 * 
 * 版本：v 1.0.0
 * 日期：2022年6月17日
 * 
 * 作者：斜影重阳xycy
 * **************************************/

using System.Diagnostics;
using HexBox;

namespace HexBox.Functions.Other
{
    internal class CApp
    {
        /// <summary>
        /// 退出应用
        /// </summary>
        internal static void Exit()
        {
            Debug.WriteLine("【CApp.Exit()】即将退出应用...");
            Environment.Exit(0);    // 完全退出应用
        }


        /// <summary>
        /// 重启应用
        /// </summary>
        internal static void Reboot()
        {
            Debug.WriteLine("【CApp.Reboot()】即将重启应用...");
            Application.Restart();  // 重启应用
        }


        /// <summary>
        /// 应用初始化
        /// </summary>
        internal static void Init(Form_home form)
        {
            Debug.WriteLine("【CApp.Init()】应用开始初始化...");

            form.Text = $"{Properties.Settings.Default.AppName_CN} ({Properties.Settings.Default.AppName_EN}) v{Properties.Settings.Default.AppVersion}";    // 标题
        }


        /// <summary>
        /// 打开工具列表
        /// </summary>
        /// <param name="form">窗体实例</param>
        internal static void OpenToolList(Form_home form)
        {
            form.panel_left.Visible = true;
            form.button_fold.Image = Properties.Resources.arrow_left_x16;
            form.展开ToolStripMenuItem.Image = Properties.Resources.check_x48;
            form.折叠ToolStripMenuItem.Image = null;
        }


        /// <summary>
        /// 折叠工具列表
        /// </summary>
        /// <param name="form">窗体实例</param>
        internal static void CloseToolList(Form_home form)
        {
            form.panel_left.Visible = false;// 隐藏侧边栏
            form.button_fold.Image = Properties.Resources.arrow_right_x16;// 修改图标

            // 修改展开、折叠的菜单按钮图标
            form.展开ToolStripMenuItem.Image = null;                     
            form.折叠ToolStripMenuItem.Image = Properties.Resources.check_x48;
        }


        /// <summary>
        /// 设置置顶
        /// </summary>
        /// <param name="form"></param>
        internal static void SetTopMost(Form_home form)
        {
            form.TopMost = true;                                                    // 设置置顶
            form.设置置顶ToolStripMenuItem.Image = Properties.Resources.check_x48;  // 给【设置置顶】添加图标
            form.取消置顶ToolStripMenuItem.Image = null;                            // 给【取消置顶】去除图标
        }


        /// <summary>
        /// 取消置顶
        /// </summary>
        /// <param name="form"></param>
        internal static void CancelTopMost(Form_home form)
        {
            form.TopMost = false;                                                   // 取消置顶
            form.设置置顶ToolStripMenuItem.Image = null;                            // 给【设置置顶】去除图标
            form.取消置顶ToolStripMenuItem.Image = Properties.Resources.check_x48;  // 给【取消置顶】添加图标
        }

        
    }
}
