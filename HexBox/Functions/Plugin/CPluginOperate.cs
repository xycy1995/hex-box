﻿/************************************************************************************************
 * 插件操作类
 * 用于载入插件等操作
 * 
 * 版本：v 1.0.1
 * 日期：2022年6月17日
 * 
 * 作者：斜影重阳xycy
 * 联系：https://space.bilibili.com/178094634
 * **********************************************************************************************/


using System.Diagnostics;
using System.Reflection;
using HexBoxPluginBase;

namespace HexBox.Functions.Plugin
{
    internal class CPluginOperate
    {
        /// <summary>
        /// 插件列表
        /// </summary>
        public List<CPluginMod> PluginList = new();



        /// <summary>
        /// 从文件加载插件（单个）
        /// </summary>
        /// <returns>返回加载成功与否</returns>
        public bool LoadPlugin(string filePath)
        {
            // 判断文件路径是否存在
            if (File.Exists(filePath))
            {
                try
                {
                    Debug.WriteLine("【CPluginOperate.LoadPlugin()】尝试加载插件...");

                    // 尝试加载
                    Assembly assembly = Assembly.LoadFrom(filePath);    // 加载dll，并加载依赖
                    Type[] types = assembly.GetTypes();                 // 获取所有类型

                    // 逐个判断类型，用以确定是不是本软件的接口类型，如果是，就创建实例，添加到插件列表里
                    foreach (Type type in types) // 判断类型
                    {
                        // 判断继承 ITool 接口（如果存在 ITool 类型）
                        if (type.GetInterface(typeof(ITool).Name) != null)
                        {
                            // 动态创建实例
                            if (Activator.CreateInstance(type) is ITool tool)
                            {
                                // 添加到插件列表
                                PluginList.Add(new CPluginMod(tool));
                                Debug.WriteLine($"【CPluginOperate.LoadPlugin()】插件 {Path.GetFileNameWithoutExtension(filePath)} 加载成功");
                                return true;
                            }
                            else
                            {
                                Debug.WriteLine("【CPluginOperate.LoadPlugin()】插件实例创建失败");
                                return false;
                            }

                        }
                        
                    }

                    // 如果上面的遍历没有符合接口要求
                    Debug.WriteLine("【CPluginOperate.LoadPlugin()】该文件不符合应用程序的插件类要求");
                    return false;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine("【CPluginOperate.LoadPlugin()】异常信息：" + ex.Message);
                    return false;
                }
            }

            // 如果文件路径不存在
            else
            {
                Debug.WriteLine("【CPluginOperate.LoadPlugin()】文件不存在");
                return false;
            }

        }




        /// <summary>
        /// 从文件夹加载插件（批量）
        /// </summary>
        /// <param name="dirPath">文件夹路径</param>
        public void LoadPlugins(string dirPath)
        {
            if (Directory.Exists(dirPath))
            {
                DirectoryInfo directoryInfo = new(dirPath);  // 实例一个DirectoryInfo

                Debug.WriteLine("【CPluginOperate.LoadPlugins()】开始遍历 ./plugins/ 下的 dll 文件...");

                foreach (FileInfo fileInfo in directoryInfo.GetFiles("*.dll"))  // 遍历所有dll文件
                {
                    Debug.WriteLine("【CPluginOperate.LoadPlugins()】发现 dll 文件：" + fileInfo.Name);
                    Debug.WriteLine("【CPluginOperate.LoadPlugins()】下面开始调用 CPluginOperate.LoadPlugin() 进行具体操作");
                    LoadPlugin(fileInfo.FullName);  // 加载 dll

                }

                Debug.WriteLine("【CPluginOperate.LoadPlugins()】遍历结束");
            }
            else
            {
                Debug.WriteLine("【CPluginOperate.LoadPlugins()】文件夹路径不存在");
            }
        }




    }
}
