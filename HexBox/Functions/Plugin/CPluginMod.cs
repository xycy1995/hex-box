﻿/************************************************************************************************
 * 插件模型类
 * 
 * 用于定义插件的模型
 * 必须包含：插件实例、插件名称、插件版本、插件描述、插件图标
 * 
 * 设计思路：
 *      1.初始化时，通过构造函数将[插件实例 plugin]赋值到[本地实例 Plugin]中;
 *      2.从[本地实例 Plugin]中将成员变量提取出来，使用时可以直接访问[CPluginMod类实例]的属性
 *
 * 版本：v 1.0.0
 * 日期：2022年6月17日
 * 
 * 作者：斜影重阳xycy
 * 联系：https://space.bilibili.com/178094634
 * *********************************************************************************************/

using HexBoxPluginBase;

namespace HexBox.Functions.Plugin
{
    public class CPluginMod
    {
        /// <summary>
        /// 加载后本地的插件实例
        /// </summary>
        private ITool Plugin { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="plugin"></param>
        public CPluginMod(ITool plugin)
        {
            // 接收 dll 插件实例
            Plugin = plugin;
        }

        /// <summary>
        /// 插件名称
        /// </summary>
        public string Name => Plugin.Name;

        /// <summary>
        /// 插件版本
        /// </summary>
        public string Version => Plugin.Version;

        /// <summary>
        /// 插件的作者
        /// </summary>
        public string Author => Plugin.Author;

        /// <summary>
        /// 插件功能描述
        /// </summary>
        public string Describe => Plugin.Describe;

        /// <summary>
        /// 插件图标
        /// </summary>
        public Icon Icon => Plugin.Icon;

        /// <summary>
        /// 创建窗口实例
        /// </summary>
        /// <returns>返回 object，需拆包为 Form 类型</returns>
        public object CreateFormInstance() => Plugin.CreateFormInstance();
    }

}
