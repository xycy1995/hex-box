﻿/*************************************************
 * 食物的模型
 * 
 * 应该有的内容：
 * 1. 属性：
 *          食物的位置
 *          食物可大小
 * 2. 方法：
 *          移动
 *          随机移动
 *          
 * **********************************************/

namespace HexBox.SubForms.Eggs.Snake.Model
{
    internal class CFood
    {
        /// <summary>
        /// 食物的坐标
        /// </summary>
        public Point Location { get; set; }

        /// <summary>
        /// 食物的大小
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="size">食物的大小</param>
        /// <param name="location">食物的位置</param>
        public CFood(Size size, Point location)
        {
            // 更新大小
            Size = size;

            // 更新坐标
            Location = location;
        }

        /// <summary>
        /// 移动到坐标
        /// </summary>
        /// <param name="location"></param>
        public void Move(Point location)
        {
            // 更新坐标
            Location = location;
        }

        /// <summary>
        /// 随机移动
        /// </summary>
        /// <param name="x">移动区域的左上角 X</param>
        /// <param name="y">移动区域的左上角 Y</param>
        /// <param name="length">移动区域的长度</param>
        /// <param name="width">移动区域的宽度</param>
        public void RandomMove(Point location, Size size)
        {
            // 随机更新坐标
            Location = new Point(new Random().Next(location.X, size.Width), new Random().Next(location.Y, size.Height));
        }


    }
}
