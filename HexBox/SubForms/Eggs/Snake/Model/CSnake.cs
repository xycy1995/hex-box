﻿/*************************************************
 * 蛇应该有的内容：
 * 1. 属性：
 *          蛇的每个块的信息（列表）
 *          
 * 2. 方法：
 *          
 * 
 * **********************************************/

namespace HexBox.SubForms.Eggs.Snake.Model
{
    internal class CSnake
    {
        /// <summary>
        /// 蛇的组成（列表形式）
        /// </summary>
        public List<CSnakePart> Parts = new();


        /// <summary>
        /// 蛇的朝向 WSAD对应上下左右
        /// </summary>
        public char Direction { get; set; }


        /// <summary>
        /// 蛇的速度（单位时间内步进的大小）
        /// </summary>
        public int Speed { get; set; }


        public CSnake()
        {
            // 创建初始蛇的样貌
            Init();
        }

        public void Init()
        {
            Parts.Clear();

            CSnakePart snakePart_head = new(true, new Point(500, 100), new Size(20, 20));
            CSnakePart snakePart_body1 = new(false, new Point(520, 100), new Size(20, 20));
            CSnakePart snakePart_body2 = new(false, new Point(540, 100), new Size(20, 20));

            Parts.Add(snakePart_head);
            Parts.Add(snakePart_body1);
            Parts.Add(snakePart_body2);

            Direction = 'A';
            Speed = 20;

        }

        /// <summary>
        /// 增加一节蛇块
        /// </summary>
        public void AddPart()
        {
            // 增加的蛇块与最后一个蛇块位置一样
            CSnakePart snakePart_body = new(false, new Point(Parts[^1].Location.X, Parts[^1].Location.Y), new Size(20, 20));
            Parts.Add(snakePart_body);
        }


        /// <summary>
        /// 蛇前进一步
        /// </summary>
        public void MoveOneStep()
        {
            // 实现蛇的移动原理：所有数据往前推
            for (int i = Parts.Count - 1; i >= 0; i--)
            {
                // 如果为蛇头
                // 第一个蛇块（蛇头）根据方向挪动
                if (i == 0)
                {
                    // 如果方向朝上
                    if (Direction == 'W')
                    {
                        // Y 方向加上蛇的步进速度
                        Parts[i].Location = new Point(Parts[i].Location.X, Parts[i].Location.Y - Speed);
                    }
                    // 如果方向朝下
                    else if (Direction == 'S')
                    {
                        Parts[i].Location = new Point(Parts[i].Location.X, Parts[i].Location.Y + Speed);
                    }
                    // 如果方向朝左
                    else if (Direction == 'A')
                    {
                        Parts[i].Location = new Point(Parts[i].Location.X - Speed, Parts[i].Location.Y);
                    }
                    // 如果方向朝右
                    else if (Direction == 'D')
                    {
                        Parts[i].Location = new Point(Parts[i].Location.X + Speed, Parts[i].Location.Y);
                    }
                }
                // 如果为蛇块
                else
                {
                    // 逆向将前一个蛇块的数据复制给后一个蛇块(整体往前挪)
                    Parts[i].Location = Parts[i - 1].Location;
                }
            }
        }


    }
}
