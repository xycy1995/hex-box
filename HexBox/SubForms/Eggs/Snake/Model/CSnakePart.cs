﻿/*************************************************
 * 蛇的基类
 * 把蛇分为一块一块的部分（包括蛇头、蛇身）
 *          
 * 
 * **********************************************/

namespace HexBox.SubForms.Eggs.Snake.Model
{
    internal class CSnakePart
    {
        /// <summary>
        /// 部分的类型，是否为蛇头
        /// </summary>
        public bool IsHead { get; set; }

        /// <summary>
        /// 蛇块的坐标
        /// </summary>
        public Point Location { get; set; }

        /// <summary>
        /// 蛇块的大小
        /// </summary>
        public Size Size { get; set; }


        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="type">部分的类型</param>
        /// <param name="image">部分的外观</param>
        /// <param name="x">部分的左上角坐标 X</param>
        /// <param name="y">部分的左上角坐标 Y</param>
        public CSnakePart(bool isHead, Point location, Size size)
        {
            // 获得蛇块的类型
            IsHead = isHead;

            // 获得蛇块的坐标
            Location = location;
            Size = size;
        }
    }
}
