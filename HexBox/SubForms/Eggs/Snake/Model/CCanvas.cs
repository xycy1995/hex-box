﻿/*************************************************
 * 贪吃蛇画布
 * 1. 属性：
 *          画布大小：int length, width
 *          画布颜色：color backgroundColor
 *          画布网格显示：bool showGrid
 *          
 * 2. 方法：
 *          设置画布大小：SetSize()
 *          锁定画布大小：Lock()
 *          设置画布颜色：SetBackgroundColor()
 *          清空画布内容：Clear()
 *          绘制战场：DrawBattlefield()
 *          绘制界面控件：DrawControls()
 * 
 * **********************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HexBox.SubForms.Eggs.Snake.Model
{
    internal class CCanvas
    {
        private int Length { get; set; }
        private int Width { get; set; }
        private Color BackgroungColor { get; set; }
        private bool ShowGrid { get; set; }
        private Panel Canvas { get; }



        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="panel"></param>
        public CCanvas(Panel panel)
        {
            Canvas = panel; // 获取画布实例
        }


        /// <summary>
        /// 清空画布
        /// </summary>
        public void Clear(Graphics g)
        {
            g.Clear(Color.White);

        }


        /// <summary>
        /// 添加食物
        /// </summary>
        /// <param name="g"></param>
        /// <param name="food"></param>
        public void FoodAdd(Graphics g, CFood food)
        {
            // 画一个红色的圆
            Brush brush = new SolidBrush(Color.Red);
            g.FillEllipse(brush, food.Location.X, food.Location.Y, food.Size.Width, food.Size.Height);
        }



        public void SnakeCreate(Graphics g, CSnake snake)
        {
            Brush brush = new SolidBrush(Color.Black);

            foreach (var part in snake.Parts)
            {
                // 如果是蛇头，就画成蛇头的样子（此处为黑色实心方块）
                if (part.IsHead)
                {
                    g.FillRectangle(brush, part.Location.X, part.Location.Y, part.Size.Width, part.Size.Height);
                }
                // 如果是蛇身，就画成蛇身的样子（此处为黑色实心圆）
                else
                {
                    g.FillEllipse(brush, part.Location.X, part.Location.Y, part.Size.Width, part.Size.Height);
                }
            }
        }

    }
}
