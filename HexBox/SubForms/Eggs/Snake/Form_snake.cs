﻿using System.Diagnostics;
using HexBox.SubForms.Eggs.Snake.Model;

namespace HexBox.SubForms.Eggs.Snake
{
    public partial class Form_snake : Form
    {
        bool isGaming = false;  // 游戏开始标志位
        int game_score = 0;     // 分数记录器

        // 实例化
        CCanvas canvas;                                             // 画布
        Bitmap bitmap = new(600, 400);                              // 用于双缓冲
        CFood food = new(new Size(20, 20), new Point(300, 100));    // 食物
        CSnake snake = new();                                       // 蛇
        Graphics g;                                                 // GDI+ 绘图
        Graphics gt;

        public Form_snake()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗口加载完毕时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_home_Load(object sender, EventArgs e)
        {
            // 界面主页
            panel_welcome.Visible = true;
            panel_gameStart.Visible = false;
            panel_gameOver.Visible = false;

        }


        private void button_start_Click(object sender, EventArgs e)
        {
            canvas = new(panel_battlefield);
            //g = panel_battlefield.CreateGraphics();
            g = Graphics.FromImage(bitmap);
            gt = panel_battlefield.CreateGraphics();

            // 对蛇、食物、得分等进行清空重置（否则会对第二次游戏产生影响）
            snake.Init();
            game_score = 0;
            label_score.Text = "0";

            // 切换到开始游戏的界面
            panel_welcome.Visible = false;
            panel_gameStart.Visible = true;
            panel_gameOver.Visible = false;

            isGaming = true;    // 游戏开始标志
            timer.Start();
            Debug.WriteLine("游戏开始");

        }


        private void button_return_Click(object sender, EventArgs e)
        {
            // 界面主页
            panel_welcome.Visible = true;
            panel_gameStart.Visible = false;
            panel_gameOver.Visible = false;
        }


        private void Form_home_KeyDown(object sender, KeyEventArgs e)
        {
            Debug.WriteLine("触发按键");

            // 只有开始游戏时按键才有效
            if (isGaming == true)
            {
                // 按下 WSAD ，修改 snake 实例中的 Direction
                switch (e.KeyCode)
                {
                    case Keys.W:
                        snake.Direction = 'W';
                        Debug.WriteLine("按下 W");
                        break;

                    case Keys.S:
                        snake.Direction = 'S';
                        Debug.WriteLine("按下 S");
                        break;

                    case Keys.A:
                        snake.Direction = 'A';
                        Debug.WriteLine("按下 A");
                        break;

                    case Keys.D:
                        snake.Direction = 'D';
                        Debug.WriteLine("按下 D");
                        break;

                    default:
                        Debug.WriteLine("不支持的按键，仅支持 WSAD");
                        break;

                }
            }
        }


        /// <summary>
        /// 检测是否吃到了食物
        /// </summary>
        /// <param name="snake"></param>
        /// <param name="food"></param>
        /// <returns>吃到了返回 true， 没吃到返回 false</returns>
        private static bool AteFood(CSnake snake, CFood food)
        {
            int snake_x = snake.Parts[0].Location.X;
            int snake_y = snake.Parts[0].Location.Y;
            int snake_width = snake.Parts[0].Size.Width;
            int snake_height = snake.Parts[0].Size.Height;
            int food_x = food.Location.X;
            int food_y = food.Location.Y;
            int food_width = food.Size.Width;
            int food_height = food.Size.Height;

            // 食物往外偏移蛇的一半大小（注意：坐标在食物的左上角）
            if ((food_x - snake_width / 2 < snake_x) && (snake_x < food_x + food_width + snake_width / 2) && (food_y - snake_height / 2 < snake_y) && (snake_y < food_y + food_height + snake_height / 2))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        /// <summary>
        /// 检测是否撞墙死亡
        /// </summary>
        /// <param name="panel">战场画布</param>
        /// <param name="snake">蛇</param>
        /// <returns>撞死了返回 true， 没撞死返回 false</returns>
        private static bool Dead(Panel panel, CSnake snake)
        {
            int battlefield_width = panel.Size.Width;
            int battlefield_height = panel.Size.Height;
            int snake_x = snake.Parts[0].Location.X;
            int snake_y = snake.Parts[0].Location.Y;
            int snake_width = snake.Parts[0].Size.Width;
            int snake_height = snake.Parts[0].Size.Height;

            if ((snake_x <= 0) || (snake_y <= 0) || (snake_x >= (battlefield_width - snake_width)) || (snake_y >= battlefield_height - snake_height))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 定时器触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            // 蛇移动一步
            snake.MoveOneStep();

            // 如果食物被吃了
            if (AteFood(snake, food))
            {
                game_score++;                                           // 增加分数
                label_score.Text = game_score.ToString();               // 修改显示分数
                snake.AddPart();                                        // 增加蛇块
                food.RandomMove(new Point(0, 0), new Size(550, 350));   // 更新食物位置
            }

            // 渲染画面
            // 先渲染画面，再出是否死亡的结果
            canvas.Clear(g);                // 清空画布内容
            canvas.FoodAdd(g, food);        // 画食物
            canvas.SnakeCreate(g, snake);   // 画蛇

            gt.DrawImage(bitmap, 0,0);

            // 如果撞死了
            if (Dead(panel_battlefield, snake))
            {
                isGaming = false;
                timer.Stop();
                label_gameover_score.Text = game_score.ToString();
                Debug.WriteLine("游戏结束，得分为：" + game_score);

                // 切换到游戏结束的界面
                panel_welcome.Visible = false;
                panel_gameStart.Visible = false;
                panel_gameOver.Visible = true;
            }
        }
    }
}
