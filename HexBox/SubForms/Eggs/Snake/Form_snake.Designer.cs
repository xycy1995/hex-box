﻿namespace HexBox.SubForms.Eggs.Snake
{
    partial class Form_snake
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_snake));
            this.button_start = new System.Windows.Forms.Button();
            this.label_title = new System.Windows.Forms.Label();
            this.pictureBox_snake = new System.Windows.Forms.PictureBox();
            this.panel_welcome = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel_gameStart = new System.Windows.Forms.Panel();
            this.label_score = new System.Windows.Forms.Label();
            this.label_scoreTag = new System.Windows.Forms.Label();
            this.panel_battlefield = new System.Windows.Forms.Panel();
            this.panel_gameOver = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button_return = new System.Windows.Forms.Button();
            this.label_gameover_score = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_snake)).BeginInit();
            this.panel_welcome.SuspendLayout();
            this.panel_gameStart.SuspendLayout();
            this.panel_gameOver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(231, 343);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(160, 40);
            this.button_start.TabIndex = 0;
            this.button_start.Text = "开始游戏";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // label_title
            // 
            this.label_title.AutoSize = true;
            this.label_title.Font = new System.Drawing.Font("Microsoft YaHei UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_title.Location = new System.Drawing.Point(206, 54);
            this.label_title.Name = "label_title";
            this.label_title.Size = new System.Drawing.Size(210, 36);
            this.label_title.TabIndex = 1;
            this.label_title.Text = "HexBox 贪吃蛇";
            // 
            // pictureBox_snake
            // 
            this.pictureBox_snake.Image = global::HexBox.Properties.Resources_eggs_snake.snake;
            this.pictureBox_snake.Location = new System.Drawing.Point(3, 131);
            this.pictureBox_snake.Name = "pictureBox_snake";
            this.pictureBox_snake.Size = new System.Drawing.Size(616, 200);
            this.pictureBox_snake.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_snake.TabIndex = 3;
            this.pictureBox_snake.TabStop = false;
            // 
            // panel_welcome
            // 
            this.panel_welcome.Controls.Add(this.label4);
            this.panel_welcome.Controls.Add(this.label3);
            this.panel_welcome.Controls.Add(this.pictureBox_snake);
            this.panel_welcome.Controls.Add(this.button_start);
            this.panel_welcome.Controls.Add(this.label_title);
            this.panel_welcome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_welcome.Location = new System.Drawing.Point(0, 0);
            this.panel_welcome.Name = "panel_welcome";
            this.panel_welcome.Size = new System.Drawing.Size(622, 463);
            this.panel_welcome.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(304, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 27);
            this.label4.TabIndex = 7;
            this.label4.Text = "v1.0.1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(231, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 27);
            this.label3.TabIndex = 6;
            this.label3.Text = "版本：";
            // 
            // panel_gameStart
            // 
            this.panel_gameStart.Controls.Add(this.label_score);
            this.panel_gameStart.Controls.Add(this.label_scoreTag);
            this.panel_gameStart.Controls.Add(this.panel_battlefield);
            this.panel_gameStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_gameStart.Location = new System.Drawing.Point(0, 0);
            this.panel_gameStart.Name = "panel_gameStart";
            this.panel_gameStart.Size = new System.Drawing.Size(622, 463);
            this.panel_gameStart.TabIndex = 4;
            // 
            // label_score
            // 
            this.label_score.AutoSize = true;
            this.label_score.Location = new System.Drawing.Point(72, 16);
            this.label_score.Name = "label_score";
            this.label_score.Size = new System.Drawing.Size(18, 20);
            this.label_score.TabIndex = 2;
            this.label_score.Text = "0";
            // 
            // label_scoreTag
            // 
            this.label_scoreTag.AutoSize = true;
            this.label_scoreTag.Location = new System.Drawing.Point(12, 16);
            this.label_scoreTag.Name = "label_scoreTag";
            this.label_scoreTag.Size = new System.Drawing.Size(54, 20);
            this.label_scoreTag.TabIndex = 2;
            this.label_scoreTag.Text = "得分：";
            // 
            // panel_battlefield
            // 
            this.panel_battlefield.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_battlefield.BackColor = System.Drawing.Color.White;
            this.panel_battlefield.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_battlefield.Location = new System.Drawing.Point(12, 51);
            this.panel_battlefield.Name = "panel_battlefield";
            this.panel_battlefield.Size = new System.Drawing.Size(600, 400);
            this.panel_battlefield.TabIndex = 1;
            // 
            // panel_gameOver
            // 
            this.panel_gameOver.Controls.Add(this.pictureBox1);
            this.panel_gameOver.Controls.Add(this.button_return);
            this.panel_gameOver.Controls.Add(this.label_gameover_score);
            this.panel_gameOver.Controls.Add(this.label2);
            this.panel_gameOver.Controls.Add(this.label1);
            this.panel_gameOver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_gameOver.Location = new System.Drawing.Point(0, 0);
            this.panel_gameOver.Name = "panel_gameOver";
            this.panel_gameOver.Size = new System.Drawing.Size(622, 463);
            this.panel_gameOver.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HexBox.Properties.Resources_eggs_snake.snake_dead;
            this.pictureBox1.Location = new System.Drawing.Point(3, 124);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(616, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // button_return
            // 
            this.button_return.Location = new System.Drawing.Point(231, 341);
            this.button_return.Name = "button_return";
            this.button_return.Size = new System.Drawing.Size(160, 40);
            this.button_return.TabIndex = 4;
            this.button_return.Text = "返回主页";
            this.button_return.UseVisualStyleBackColor = true;
            this.button_return.Click += new System.EventHandler(this.button_return_Click);
            // 
            // label_gameover_score
            // 
            this.label_gameover_score.AutoSize = true;
            this.label_gameover_score.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_gameover_score.Location = new System.Drawing.Point(328, 94);
            this.label_gameover_score.Name = "label_gameover_score";
            this.label_gameover_score.Size = new System.Drawing.Size(24, 27);
            this.label_gameover_score.TabIndex = 5;
            this.label_gameover_score.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(250, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 27);
            this.label2.TabIndex = 5;
            this.label2.Text = "得分：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(238, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 36);
            this.label1.TabIndex = 5;
            this.label1.Text = "游 戏 结 束";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Form_snake
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 463);
            this.Controls.Add(this.panel_welcome);
            this.Controls.Add(this.panel_gameOver);
            this.Controls.Add(this.panel_gameStart);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(640, 510);
            this.MinimumSize = new System.Drawing.Size(640, 510);
            this.Name = "Form_snake";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HexBox 贪吃蛇";
            this.Load += new System.EventHandler(this.Form_home_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_home_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_snake)).EndInit();
            this.panel_welcome.ResumeLayout(false);
            this.panel_welcome.PerformLayout();
            this.panel_gameStart.ResumeLayout(false);
            this.panel_gameStart.PerformLayout();
            this.panel_gameOver.ResumeLayout(false);
            this.panel_gameOver.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Button button_start;
        private Label label_title;
        private PictureBox pictureBox_snake;
        private Panel panel_welcome;
        private Panel panel_gameStart;
        private Panel panel_gameOver;
        private PictureBox pictureBox1;
        private Button button_return;
        private Label label1;
        private Panel panel_battlefield;
        private Label label_score;
        private Label label_scoreTag;
        private System.Windows.Forms.Timer timer;
        private Label label_gameover_score;
        private Label label2;
        private Label label4;
        private Label label3;
    }
}