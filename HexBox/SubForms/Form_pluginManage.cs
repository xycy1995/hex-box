﻿/***************************************************
 * 插件管理界面
 * 该界面是彩蛋触发界面,操作方式：
 *      先按 Esc ，然后 ↑↑↓↓←→←→BABA，最后 Enter
 * 然后就会打开彩蛋（不以插件形式）
 * 
 * ************************************************/

using HexBox.Functions.Plugin;
using System.Data;
using System.Diagnostics;

namespace HexBox.SubForms
{
    public partial class Form_pluginManage : Form
    {
        // 用于记录按键
        List<Keys> KeysList = new();

        public Form_pluginManage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载完毕后的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FPluginManage_Load(object sender, EventArgs e)
        {
            PluginsShow();  // 更新插件显示
        }

        /// <summary>
        /// 更新显示插件文件夹下的插件
        /// </summary>
        private void PluginsShow()
        {
            // 载入插件文件夹下面的插件，并显示到列表中
            string pluginsPath = Path.GetFullPath(Properties.Settings.Default.PluginsDirPath);   // 获取插件文件夹绝对路径

            // 实例化插件操作类
            CPluginOperate po = new();
            // 载入插件
            po.LoadPlugins(pluginsPath);


            // 根据得到插件数据，创建表格内容
            // 先创建列
            DataTable dataTable = new();
            DataColumn column_name = new("名称");
            DataColumn column_version = new("版本");
            DataColumn column_author = new("作者");
            DataColumn column_describe = new("描述");
            dataTable.Columns.Add(column_name);
            dataTable.Columns.Add(column_version);
            dataTable.Columns.Add(column_author);
            dataTable.Columns.Add(column_describe);

            // 再创建行
            foreach (var plugin in po.PluginList)
            {
                dataTable.Rows.Add(plugin.Name, plugin.Version, plugin.Author, plugin.Describe);
            }

            // 将表格绑定到 DataGridView
            dataGridView_plugin.DataSource = dataTable;

            // 更新状态栏插件数量提示
            toolStripStatusLabel_pluginNum.Text = po.PluginList.Count.ToString();
        }


        /// <summary>
        /// 根据输入框里的内容改变[加载插件]按钮的可用性
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_path_TextChanged(object sender, EventArgs e)
        {
            // 如果没有内容，失能按钮
            if (textBox_path.Text == "")
            {
                button_importPlugin.Enabled = false;
            }
            // 如果有内容，使能按钮
            else
            {
                button_importPlugin.Enabled = true;
            }
        }


        private void button_selectPath_Click(object sender, EventArgs e)
        {
            // 选择插件
            if (openFileDialog_plugin.ShowDialog() == DialogResult.OK)
            {
                textBox_path.Text = openFileDialog_plugin.FileName; // 显示插件路径
            }
        }

        private void button_importPlugin_Click(object sender, EventArgs e)
        {
            // 获得插件文件夹的绝对路径
            string descDirPath = Path.GetFullPath(Properties.Settings.Default.PluginsDirPath);

            // 获得选择路径的文件名
            FileInfo sourceFile = new(textBox_path.Text);
            string sourceFileName = sourceFile.Name;

            // 如果插件文件夹不存在
            if (!Directory.Exists(descDirPath))
            {
                // 则先创建插件文件夹
                Directory.CreateDirectory(descDirPath);
            }

            // 如果插件文件已存在
            if (File.Exists(descDirPath + sourceFileName))
            {
                // 先询问是否替换原文件
                if (MessageBox.Show("插件已存在，是否替换？", "注意", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    try
                    {
                        // 将选择的插件复制到插件文件夹
                        File.Copy(textBox_path.Text, descDirPath + sourceFileName, true);
                    }
                    catch
                    {
                        MessageBox.Show("导入失败，请检查文件后再试。", "注意", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    // 否则结束当前函数
                    return;
                }

            }
            else
            {
                // 如果不存在，直接复制到插件文件夹
                File.Copy(textBox_path.Text, descDirPath + sourceFileName);
            }

            PluginsShow();  // 更新插件显示

            // 弹出提示框，询问是否重启
            if (MessageBox.Show("插件导入成功，重启应用后生效，是否重启？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Application.Restart();
            }

        }

        private void 打开插件文件夹ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 获取绝对路径
            string path = Directory.GetCurrentDirectory() + Properties.Settings.Default.PluginsDirPath_Abs;

            // 如果插件文件夹不存在，就创建文件夹
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // 打开文件夹
            Process.Start("explorer.exe", path);
        }

        private void Form_pluginManage_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    KeysList.Clear();   // 清空按键记录列表
                    Debug.WriteLine("重置按键记录列表");
                    break;
                case Keys.Up:
                    KeysList.Add(Keys.Up);
                    Debug.WriteLine($"按下：{Keys.Up}");
                    break;
                case Keys.Down:
                    KeysList.Add(Keys.Down);
                    Debug.WriteLine($"按下：{Keys.Down}");
                    break;
                case Keys.Left:
                    KeysList.Add(Keys.Left);
                    Debug.WriteLine($"按下：{Keys.Left}");
                    break;
                case Keys.Right:
                    KeysList.Add(Keys.Right);
                    Debug.WriteLine($"按下：{Keys.Right}");
                    break;
                case Keys.B:
                    KeysList.Add(Keys.B);
                    Debug.WriteLine($"按下：{Keys.B}");
                    break;
                case Keys.A:
                    KeysList.Add(Keys.A);
                    Debug.WriteLine($"按下：{Keys.A}");
                    break;
                case Keys.Enter:
                    // 显示按键列表内容
                    string keysList = "运行秘籍:";
                    foreach (var key in KeysList)
                    {
                        keysList += key; 
                        keysList += " ";
                    }
                    Debug.WriteLine(keysList);

                    // 判断是否符合秘籍要求
                    if ((KeysList.Count == 12) && (KeysList[0] == Keys.Up) && (KeysList[1] == Keys.Up) && (KeysList[2] == Keys.Down) && (KeysList[3] == Keys.Down) && (KeysList[4] == Keys.Left) && (KeysList[5] == Keys.Right) && (KeysList[6] == Keys.Left) && (KeysList[7] == Keys.Right) && (KeysList[8] == Keys.B) && (KeysList[9] == Keys.A) && (KeysList[10] == Keys.B) && (KeysList[11] == Keys.A))
                    {
                        // 弹出提示框
                        Debug.WriteLine("秘籍运行成功");
                        MessageBox.Show("恭喜你，发现彩蛋！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // 加载彩蛋
                        Eggs.Snake.Form_snake form_Snake = new();
                        form_Snake.ShowDialog();
                    }
                    else
                    {
                        Debug.WriteLine("秘籍无效，运行失败");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
