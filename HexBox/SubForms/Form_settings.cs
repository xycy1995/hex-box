﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HexBox.SubForms
{
    public partial class Form_settings : Form
    {
        public Form_settings()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载时，初始化界面控件状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_settings_Load(object sender, EventArgs e)
        {
            // 加载配置状态
            checkBox_exitAsk.Checked = Properties.Settings.Default.ExitAsk; // 退出应用时询问
        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_cancel_Click(object sender, EventArgs e)
        {
            // 直接退出
            Close();
        }


        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_ok_Click(object sender, EventArgs e)
        {
            // 将所有设置修改到设置文件
            Properties.Settings.Default.ExitAsk = checkBox_exitAsk.Checked;

            // 保存并退出
            Properties.Settings.Default.Save();
            MessageBox.Show("设置完成，部分设置需要重启后生效。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }

    }
}
