﻿namespace HexBox.SubForms
{
    partial class Form_pluginManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialog_plugin = new System.Windows.Forms.OpenFileDialog();
            this.textBox_path = new System.Windows.Forms.TextBox();
            this.button_selectPath = new System.Windows.Forms.Button();
            this.button_importPlugin = new System.Windows.Forms.Button();
            this.dataGridView_plugin = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip_list = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.打开插件文件夹ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_pluginNum = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_plugin)).BeginInit();
            this.contextMenuStrip_list.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 31);
            this.label2.TabIndex = 0;
            this.label2.Text = "插件路径";
            // 
            // openFileDialog_plugin
            // 
            this.openFileDialog_plugin.FileName = "openFileDialog1";
            this.openFileDialog_plugin.Filter = "dll 文件|*.dll";
            // 
            // textBox_path
            // 
            this.textBox_path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_path.Location = new System.Drawing.Point(135, 9);
            this.textBox_path.Margin = new System.Windows.Forms.Padding(5);
            this.textBox_path.Name = "textBox_path";
            this.textBox_path.ReadOnly = true;
            this.textBox_path.Size = new System.Drawing.Size(432, 38);
            this.textBox_path.TabIndex = 0;
            this.textBox_path.TextChanged += new System.EventHandler(this.textBox_path_TextChanged);
            // 
            // button_selectPath
            // 
            this.button_selectPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_selectPath.Location = new System.Drawing.Point(579, 8);
            this.button_selectPath.Margin = new System.Windows.Forms.Padding(5);
            this.button_selectPath.Name = "button_selectPath";
            this.button_selectPath.Size = new System.Drawing.Size(146, 45);
            this.button_selectPath.TabIndex = 1;
            this.button_selectPath.Text = "选择";
            this.button_selectPath.UseVisualStyleBackColor = true;
            this.button_selectPath.Click += new System.EventHandler(this.button_selectPath_Click);
            // 
            // button_importPlugin
            // 
            this.button_importPlugin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_importPlugin.Enabled = false;
            this.button_importPlugin.Location = new System.Drawing.Point(734, 8);
            this.button_importPlugin.Margin = new System.Windows.Forms.Padding(5);
            this.button_importPlugin.Name = "button_importPlugin";
            this.button_importPlugin.Size = new System.Drawing.Size(146, 45);
            this.button_importPlugin.TabIndex = 2;
            this.button_importPlugin.Text = "导入";
            this.button_importPlugin.UseVisualStyleBackColor = true;
            this.button_importPlugin.Click += new System.EventHandler(this.button_importPlugin_Click);
            // 
            // dataGridView_plugin
            // 
            this.dataGridView_plugin.AllowUserToAddRows = false;
            this.dataGridView_plugin.AllowUserToDeleteRows = false;
            this.dataGridView_plugin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_plugin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_plugin.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_plugin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_plugin.ContextMenuStrip = this.contextMenuStrip_list;
            this.dataGridView_plugin.Location = new System.Drawing.Point(20, 62);
            this.dataGridView_plugin.Margin = new System.Windows.Forms.Padding(5);
            this.dataGridView_plugin.MultiSelect = false;
            this.dataGridView_plugin.Name = "dataGridView_plugin";
            this.dataGridView_plugin.ReadOnly = true;
            this.dataGridView_plugin.RowHeadersWidth = 51;
            this.dataGridView_plugin.RowTemplate.Height = 29;
            this.dataGridView_plugin.Size = new System.Drawing.Size(860, 414);
            this.dataGridView_plugin.TabIndex = 0;
            // 
            // contextMenuStrip_list
            // 
            this.contextMenuStrip_list.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip_list.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开插件文件夹ToolStripMenuItem});
            this.contextMenuStrip_list.Name = "contextMenuStrip_list";
            this.contextMenuStrip_list.Size = new System.Drawing.Size(257, 42);
            // 
            // 打开插件文件夹ToolStripMenuItem
            // 
            this.打开插件文件夹ToolStripMenuItem.Name = "打开插件文件夹ToolStripMenuItem";
            this.打开插件文件夹ToolStripMenuItem.Size = new System.Drawing.Size(256, 38);
            this.打开插件文件夹ToolStripMenuItem.Text = "打开插件文件夹";
            this.打开插件文件夹ToolStripMenuItem.Click += new System.EventHandler(this.打开插件文件夹ToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel_pluginNum});
            this.statusStrip1.Location = new System.Drawing.Point(0, 480);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 22, 0);
            this.statusStrip1.Size = new System.Drawing.Size(899, 41);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(134, 31);
            this.toolStripStatusLabel1.Text = "插件数量：";
            // 
            // toolStripStatusLabel_pluginNum
            // 
            this.toolStripStatusLabel_pluginNum.Name = "toolStripStatusLabel_pluginNum";
            this.toolStripStatusLabel_pluginNum.Size = new System.Drawing.Size(28, 31);
            this.toolStripStatusLabel_pluginNum.Text = "0";
            // 
            // Form_pluginManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 521);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button_importPlugin);
            this.Controls.Add(this.button_selectPath);
            this.Controls.Add(this.textBox_path);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView_plugin);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Form_pluginManage";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "插件管理";
            this.Load += new System.EventHandler(this.FPluginManage_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_pluginManage_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_plugin)).EndInit();
            this.contextMenuStrip_list.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Label label2;
        private OpenFileDialog openFileDialog_plugin;
        private TextBox textBox_path;
        private Button button_selectPath;
        private Button button_importPlugin;
        private DataGridView dataGridView_plugin;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripStatusLabel toolStripStatusLabel_pluginNum;
        private ContextMenuStrip contextMenuStrip_list;
        private ToolStripMenuItem 打开插件文件夹ToolStripMenuItem;
    }
}