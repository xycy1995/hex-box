﻿using System.Diagnostics;

namespace HexBox.SubForms
{
    public partial class Form_about : Form
    {
        public Form_about()
        {
            InitializeComponent();

            // 界面文字初始化
            linkLabel_version.Text = Properties.Settings.Default.AppVersion;
            label_describe.Text = Properties.Settings.Default.AppDescribe;
            label_name_cn.Text = Properties.Settings.Default.AppName_CN;
            label_name_en.Text = Properties.Settings.Default.AppName_EN;

            // 加载更新说明
            textBox_log.Text = Properties.Resources.update_log;
        }

        private void linkLabel_license_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                // 启动默认浏览器
                Process.Start("explorer.exe", Properties.Settings.Default.LicenseUrl);
            }
            catch
            {
                // 复制地址到剪切板
                Clipboard.SetText(Properties.Settings.Default.AuthorSpace);

                MessageBox.Show("打开浏览器失败，已经复制地址到剪贴板，请手动操作。", "注意", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void linkLabel_author_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                // 启动默认浏览器
                Process.Start("explorer.exe", Properties.Settings.Default.AuthorSpace);
            }
            catch
            {
                // 复制地址到剪切板
                Clipboard.SetText(Properties.Settings.Default.AuthorSpace);

                MessageBox.Show("打开浏览器失败，已经复制地址到剪贴板，请手动操作。", "注意", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void linkLabel_version_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                // 启动默认浏览器
                Process.Start("explorer.exe", Properties.Settings.Default.UpdateUrl);
            }
            catch
            {
                // 复制地址到剪切板
                Clipboard.SetText(Properties.Settings.Default.AuthorSpace);

                MessageBox.Show("打开浏览器失败，已经复制地址到剪贴板，请手动操作。", "注意", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
