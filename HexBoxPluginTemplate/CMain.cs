﻿using PluginBase;   // 引入接口

namespace HexBoxPluginTemplate
{
    public class CMain : ITool
    {
        public string Name => "这是插件的名称";              // 【必填】这是插件的名称

        public string Version => "1.0.0";                    // 【必填】这是插件的版本号

        public string Describe => "这是插件的描述";          // 【必填】这是插件的描述

        public Icon Icon => Properties.Resources.AppIcon;    // 【不改】这是插件的图标

        public object CreateFormInstance()                   // 【不改】这是插件创建窗体的功能
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }
    }
}


