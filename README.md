# HexBox

#### 介绍
六角魔盒（HexBox），一款方便学生学习计算机的应用软件。

#### 软件架构
- 框架：.Net 6.0
- 语言：C# 10.0
- 界面：WinForm
- 工具：Visual Studio 2022

#### 使用说明

详见项目 Wiki。



