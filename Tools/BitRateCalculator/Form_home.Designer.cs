﻿namespace BitRateCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown_source = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_source = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_result = new System.Windows.Forms.ComboBox();
            this.button_calc = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label_result = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_source)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDown_source
            // 
            this.numericUpDown_source.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_source.DecimalPlaces = 6;
            this.numericUpDown_source.Location = new System.Drawing.Point(89, 170);
            this.numericUpDown_source.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_source.Name = "numericUpDown_source";
            this.numericUpDown_source.Size = new System.Drawing.Size(103, 27);
            this.numericUpDown_source.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "原始速率";
            // 
            // comboBox_source
            // 
            this.comboBox_source.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_source.FormattingEnabled = true;
            this.comboBox_source.Items.AddRange(new object[] {
            "bps",
            "Kbps",
            "Mbps",
            "Gbps"});
            this.comboBox_source.Location = new System.Drawing.Point(198, 169);
            this.comboBox_source.Name = "comboBox_source";
            this.comboBox_source.Size = new System.Drawing.Size(76, 28);
            this.comboBox_source.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "计算结果";
            // 
            // comboBox_result
            // 
            this.comboBox_result.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_result.FormattingEnabled = true;
            this.comboBox_result.Items.AddRange(new object[] {
            "B/s",
            "KB/s",
            "MB/s",
            "GB/s"});
            this.comboBox_result.Location = new System.Drawing.Point(198, 119);
            this.comboBox_result.Name = "comboBox_result";
            this.comboBox_result.Size = new System.Drawing.Size(76, 28);
            this.comboBox_result.TabIndex = 1;
            // 
            // button_calc
            // 
            this.button_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_calc.Location = new System.Drawing.Point(12, 208);
            this.button_calc.Name = "button_calc";
            this.button_calc.Size = new System.Drawing.Size(262, 40);
            this.button_calc.TabIndex = 4;
            this.button_calc.Text = "计算";
            this.button_calc.UseVisualStyleBackColor = true;
            this.button_calc.Click += new System.EventHandler(this.button_calc_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(123, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "计算单位";
            // 
            // label_result
            // 
            this.label_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_result.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_result.Location = new System.Drawing.Point(12, 29);
            this.label_result.Name = "label_result";
            this.label_result.Size = new System.Drawing.Size(262, 87);
            this.label_result.TabIndex = 0;
            this.label_result.Text = "0";
            this.label_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 260);
            this.Controls.Add(this.label_result);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button_calc);
            this.Controls.Add(this.comboBox_result);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox_source);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown_source);
            this.MinimumSize = new System.Drawing.Size(302, 307);
            this.Name = "Form_home";
            this.Text = "传输速率计算";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_source)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NumericUpDown numericUpDown_source;
        private Label label1;
        private ComboBox comboBox_source;
        private Label label2;
        private ComboBox comboBox_result;
        private Button button_calc;
        private Label label5;
        private Label label_result;
    }
}