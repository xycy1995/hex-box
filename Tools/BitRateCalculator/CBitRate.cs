﻿namespace BitRateCalculator
{
    internal class CBitRate
    {
        internal static decimal Convert(decimal source_num, string source_unit, string target_unit)
        {
            // 用于存储计算结果
            decimal result = 0;

            // 根据不同的单位进行计算
            if (source_unit == "bps")
            {
                if (target_unit == "B/s")
                {
                    // number/8
                    result = source_num / 8;
                }
                else if (target_unit == "KB/s")
                {
                    // number/8/1024
                    result = source_num / 8192;
                }
                else if (target_unit == "MB/s")
                {
                    // number/8/1024/1024
                    result = source_num / 8388608;
                }
                else if (target_unit == "GB/s")
                {
                    // number/8/1024/1024/1024
                    result = source_num / 8589934592;
                }
            }
            else if (source_unit == "Kbps")
            {
                if (target_unit == "B/s")
                {
                    // number * 1024 / 8
                    result = source_num * 128;
                }
                else if (target_unit == "KB/s")
                {
                    // number * 1024 / 8 / 1024
                    result = source_num / 8;
                }
                else if (target_unit == "MB/s")
                {
                    // number * 1024 / 8 / 1024 / 1024
                    result = source_num / 8192;
                }
                else if (target_unit == "GB/s")
                {
                    result = source_num / 8388608;
                }
            }
            else if (source_unit == "Mbps")
            {
                if (target_unit == "B/s")
                {
                    // number * 1024 * 1024 / 8
                    result = source_num * 131072;
                }
                else if (target_unit == "KB/s")
                {
                    // number * 1024 * 1024 / 8 / 1024
                    result = source_num * 128;
                }
                else if (target_unit == "MB/s")
                {
                    // number * 1024 * 1024 / 8 / 1024 / 1024
                    result = source_num / 8;
                }
                else if (target_unit == "GB/s")
                {
                    // number * 1024 * 1024 / 8 / 1024 / 1024 / 1024
                    result = source_num / 8192;
                }
            }
            else if (source_unit == "Gbps")
            {
                if (target_unit == "B/s")
                {
                    // number * 1024 * 1024 * 1024 / 8
                    result = source_num * 134217728;
                }
                else if (target_unit == "KB/s")
                {
                    // number * 1024 * 1024 * 1024 / 8 / 1024
                    result = source_num * 131072;
                }
                else if (target_unit == "MB/s")
                {
                    // number * 1024 * 1024 * 1024 / 8 / 1024 / 1024
                    result = source_num * 128;
                }
                else if (target_unit == "GB/s")
                {
                    // number * 1024 * 1024 * 1024 / 8 / 1024 / 1024 / 1024
                    result = source_num / 8;
                }
            }

            return result;

        }

    }
}
