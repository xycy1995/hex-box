﻿using HexBoxPluginBase;

namespace BitRateCalculator
{
    public class CMain : ITool
    {
        public string Name => "传输速率计算器";

        public string Version => "1.0.1";
        public string Author => "斜影重阳xycy";

        public string Describe => "传输速率不同单位下的转换";

        public Icon Icon => Properties.Resources.AppIcon;

        public object CreateFormInstance()
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }

    }
}
