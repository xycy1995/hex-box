﻿using System.Diagnostics;

/*******************************************************************
 * 
 * UI控件：用Button控件显示结果数值，主要方便把文字控制在控件的右下角 
 * 
 ******************************************************************/

namespace BitRateCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
            comboBox_source.SelectedIndex = 0;
            comboBox_result.SelectedIndex = 0;
        }


        private void button_calc_Click(object sender, EventArgs e)
        {
            // 计算转换后的数值
            decimal result = CBitRate.Convert(numericUpDown_source.Value, comboBox_source.Text, comboBox_result.Text);

            // 更新界面数据
            label_result.Text = result.ToString();
            Debug.WriteLine("【BitRate】计算结果：" + result);
        }

    }
}
