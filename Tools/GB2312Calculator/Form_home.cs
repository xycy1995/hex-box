﻿using System.Diagnostics;

namespace GB2312Calculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
        }

       
        private void button_JNbyGB_calc_Click(object sender, EventArgs e)
        {
            // 计算得到结果（十进制）
            int result = CGB2312.Calc_JNbyGB((int)numericUpDown_JNbyGB_GB.Value);

            // 显示结果（大写字符串）
            label1_JNbyGB_result.Text = Convert.ToString(result, 16).ToUpper();

        }

        private void button_GBbyQW_calc_Click(object sender, EventArgs e)
        {
            // 计算得到结果（十进制）
            int result = CGB2312.Calc_GBbyQW((int)numericUpDown_GBbyQW_QW.Value);

            // 显示结果（大写字符串）
            label_GBbyQW_result.Text = Convert.ToString(result, 16).ToUpper();

        }

        private void button_JNbyQW_calc_Click(object sender, EventArgs e)
        {
            // 计算得到结果（十进制）
            int result = CGB2312.Calc_JNbyQW((int)numericUpDown_JNbyQW_QW.Value);

            // 显示结果（大写字符串）
            label_JNbyQW_result.Text = Convert.ToString(result, 16).ToUpper();
        }

        private void button_compare_calc_Click(object sender, EventArgs e)
        {
            int result = CGB2312.Calc_compare(textBox_compare_chn1.Text, textBox_compare_chn2.Text);

            if (result == 0)
            {
                label_compare_result.Text = "汉字1 = 汉字2";
            }
            else if (result == 1)
            {
                label_compare_result.Text = "汉字1 在 汉字2 前面";
            }
            else if (result == -1)
            {
                label_compare_result.Text = "汉字1 在 汉字2 后面";
            }

        }
    }
}
