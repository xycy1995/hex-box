﻿namespace GB2312Calculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.numericUpDown_JNbyGB_GB = new System.Windows.Forms.NumericUpDown();
            this.button_JNbyGB_calc = new System.Windows.Forms.Button();
            this.label1_JNbyGB_result = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.numericUpDown_GBbyQW_QW = new System.Windows.Forms.NumericUpDown();
            this.button_GBbyQW_calc = new System.Windows.Forms.Button();
            this.label_GBbyQW_result = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.numericUpDown_JNbyQW_QW = new System.Windows.Forms.NumericUpDown();
            this.button_JNbyQW_calc = new System.Windows.Forms.Button();
            this.label_JNbyQW_result = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button_compare_calc = new System.Windows.Forms.Button();
            this.textBox_compare_chn2 = new System.Windows.Forms.TextBox();
            this.textBox_compare_chn1 = new System.Windows.Forms.TextBox();
            this.label_compare_result = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_JNbyGB_GB)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_GBbyQW_QW)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_JNbyQW_QW)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(492, 312);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.numericUpDown_JNbyGB_GB);
            this.tabPage1.Controls.Add(this.button_JNbyGB_calc);
            this.tabPage1.Controls.Add(this.label1_JNbyGB_result);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(484, 279);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "国标码 转 机内码";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // numericUpDown_JNbyGB_GB
            // 
            this.numericUpDown_JNbyGB_GB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_JNbyGB_GB.Hexadecimal = true;
            this.numericUpDown_JNbyGB_GB.Location = new System.Drawing.Point(156, 165);
            this.numericUpDown_JNbyGB_GB.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_JNbyGB_GB.Name = "numericUpDown_JNbyGB_GB";
            this.numericUpDown_JNbyGB_GB.Size = new System.Drawing.Size(322, 27);
            this.numericUpDown_JNbyGB_GB.TabIndex = 20;
            // 
            // button_JNbyGB_calc
            // 
            this.button_JNbyGB_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_JNbyGB_calc.Location = new System.Drawing.Point(6, 236);
            this.button_JNbyGB_calc.Name = "button_JNbyGB_calc";
            this.button_JNbyGB_calc.Size = new System.Drawing.Size(472, 40);
            this.button_JNbyGB_calc.TabIndex = 19;
            this.button_JNbyGB_calc.Text = "计算";
            this.button_JNbyGB_calc.UseVisualStyleBackColor = true;
            this.button_JNbyGB_calc.Click += new System.EventHandler(this.button_JNbyGB_calc_Click);
            // 
            // label1_JNbyGB_result
            // 
            this.label1_JNbyGB_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1_JNbyGB_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1_JNbyGB_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1_JNbyGB_result.Location = new System.Drawing.Point(6, 33);
            this.label1_JNbyGB_result.Name = "label1_JNbyGB_result";
            this.label1_JNbyGB_result.Size = new System.Drawing.Size(472, 119);
            this.label1_JNbyGB_result.TabIndex = 16;
            this.label1_JNbyGB_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 167);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 20);
            this.label12.TabIndex = 17;
            this.label12.Text = "国标码（十六进制）";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(144, 20);
            this.label13.TabIndex = 18;
            this.label13.Text = "机内码（十六进制）";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.numericUpDown_GBbyQW_QW);
            this.tabPage2.Controls.Add(this.button_GBbyQW_calc);
            this.tabPage2.Controls.Add(this.label_GBbyQW_result);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(484, 279);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "区位码 转 国标码";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // numericUpDown_GBbyQW_QW
            // 
            this.numericUpDown_GBbyQW_QW.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_GBbyQW_QW.Location = new System.Drawing.Point(141, 165);
            this.numericUpDown_GBbyQW_QW.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_GBbyQW_QW.Name = "numericUpDown_GBbyQW_QW";
            this.numericUpDown_GBbyQW_QW.Size = new System.Drawing.Size(337, 27);
            this.numericUpDown_GBbyQW_QW.TabIndex = 15;
            // 
            // button_GBbyQW_calc
            // 
            this.button_GBbyQW_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_GBbyQW_calc.Location = new System.Drawing.Point(6, 236);
            this.button_GBbyQW_calc.Name = "button_GBbyQW_calc";
            this.button_GBbyQW_calc.Size = new System.Drawing.Size(472, 40);
            this.button_GBbyQW_calc.TabIndex = 14;
            this.button_GBbyQW_calc.Text = "计算";
            this.button_GBbyQW_calc.UseVisualStyleBackColor = true;
            this.button_GBbyQW_calc.Click += new System.EventHandler(this.button_GBbyQW_calc_Click);
            // 
            // label_GBbyQW_result
            // 
            this.label_GBbyQW_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_GBbyQW_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_GBbyQW_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_GBbyQW_result.Location = new System.Drawing.Point(6, 33);
            this.label_GBbyQW_result.Name = "label_GBbyQW_result";
            this.label_GBbyQW_result.Size = new System.Drawing.Size(472, 119);
            this.label_GBbyQW_result.TabIndex = 11;
            this.label_GBbyQW_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 167);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 20);
            this.label9.TabIndex = 12;
            this.label9.Text = "区位码（十进制）";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 20);
            this.label10.TabIndex = 13;
            this.label10.Text = "国标码（十六进制）";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.numericUpDown_JNbyQW_QW);
            this.tabPage3.Controls.Add(this.button_JNbyQW_calc);
            this.tabPage3.Controls.Add(this.label_JNbyQW_result);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(484, 279);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "区位码 转 机内码";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // numericUpDown_JNbyQW_QW
            // 
            this.numericUpDown_JNbyQW_QW.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_JNbyQW_QW.Location = new System.Drawing.Point(141, 165);
            this.numericUpDown_JNbyQW_QW.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_JNbyQW_QW.Name = "numericUpDown_JNbyQW_QW";
            this.numericUpDown_JNbyQW_QW.Size = new System.Drawing.Size(337, 27);
            this.numericUpDown_JNbyQW_QW.TabIndex = 10;
            // 
            // button_JNbyQW_calc
            // 
            this.button_JNbyQW_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_JNbyQW_calc.Location = new System.Drawing.Point(6, 236);
            this.button_JNbyQW_calc.Name = "button_JNbyQW_calc";
            this.button_JNbyQW_calc.Size = new System.Drawing.Size(472, 40);
            this.button_JNbyQW_calc.TabIndex = 9;
            this.button_JNbyQW_calc.Text = "计算";
            this.button_JNbyQW_calc.UseVisualStyleBackColor = true;
            this.button_JNbyQW_calc.Click += new System.EventHandler(this.button_JNbyQW_calc_Click);
            // 
            // label_JNbyQW_result
            // 
            this.label_JNbyQW_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JNbyQW_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_JNbyQW_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_JNbyQW_result.Location = new System.Drawing.Point(6, 33);
            this.label_JNbyQW_result.Name = "label_JNbyQW_result";
            this.label_JNbyQW_result.Size = new System.Drawing.Size(472, 119);
            this.label_JNbyQW_result.TabIndex = 3;
            this.label_JNbyQW_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 20);
            this.label7.TabIndex = 5;
            this.label7.Text = "区位码（十进制）";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 20);
            this.label8.TabIndex = 6;
            this.label8.Text = "机内码（十六进制）";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button_compare_calc);
            this.tabPage4.Controls.Add(this.textBox_compare_chn2);
            this.tabPage4.Controls.Add(this.textBox_compare_chn1);
            this.tabPage4.Controls.Add(this.label_compare_result);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(484, 279);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "汉字比较";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button_compare_calc
            // 
            this.button_compare_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_compare_calc.Location = new System.Drawing.Point(6, 233);
            this.button_compare_calc.Name = "button_compare_calc";
            this.button_compare_calc.Size = new System.Drawing.Size(472, 40);
            this.button_compare_calc.TabIndex = 2;
            this.button_compare_calc.Text = "计算";
            this.button_compare_calc.UseVisualStyleBackColor = true;
            this.button_compare_calc.Click += new System.EventHandler(this.button_compare_calc_Click);
            // 
            // textBox_compare_chn2
            // 
            this.textBox_compare_chn2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_compare_chn2.Location = new System.Drawing.Point(60, 189);
            this.textBox_compare_chn2.MaxLength = 999999999;
            this.textBox_compare_chn2.Name = "textBox_compare_chn2";
            this.textBox_compare_chn2.Size = new System.Drawing.Size(418, 27);
            this.textBox_compare_chn2.TabIndex = 1;
            // 
            // textBox_compare_chn1
            // 
            this.textBox_compare_chn1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_compare_chn1.Location = new System.Drawing.Point(60, 156);
            this.textBox_compare_chn1.MaxLength = 999999999;
            this.textBox_compare_chn1.Name = "textBox_compare_chn1";
            this.textBox_compare_chn1.Size = new System.Drawing.Size(418, 27);
            this.textBox_compare_chn1.TabIndex = 1;
            // 
            // label_compare_result
            // 
            this.label_compare_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_compare_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_compare_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_compare_result.Location = new System.Drawing.Point(6, 33);
            this.label_compare_result.Name = "label_compare_result";
            this.label_compare_result.Size = new System.Drawing.Size(472, 107);
            this.label_compare_result.TabIndex = 0;
            this.label_compare_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "汉字2";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "汉字1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "计算结果";
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 336);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form_home";
            this.Text = "中文字符转换";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_JNbyGB_GB)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_GBbyQW_QW)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_JNbyQW_QW)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private Button button_compare_calc;
        private TextBox textBox_compare_chn1;
        private Label label_compare_result;
        private Label label3;
        private Label label1;
        private Button button_JNbyQW_calc;
        private Label label_JNbyQW_result;
        private Label label7;
        private Label label8;
        private TextBox textBox_compare_chn2;
        private Label label4;
        private NumericUpDown numericUpDown_JNbyQW_QW;
        private NumericUpDown numericUpDown_GBbyQW_QW;
        private Button button_GBbyQW_calc;
        private Label label_GBbyQW_result;
        private Label label9;
        private Label label10;
        private NumericUpDown numericUpDown_JNbyGB_GB;
        private Button button_JNbyGB_calc;
        private Label label1_JNbyGB_result;
        private Label label12;
        private Label label13;
    }
}