﻿using System.Diagnostics;


namespace GB2312Calculator
{
    internal class CGB2312
    {
        /// <summary>
        /// 由国标码计算机内码
        /// </summary>
        /// <param name="gb">国标码（十六进制）</param>
        /// <returns>机内码</returns>
        static internal int Calc_JNbyGB(int gb)
        {
            int ret = gb + 0x8080;
            return ret;
        }


        /// <summary>
        /// 由区位码计算国标码
        /// </summary>
        /// <param name="qw">区位码（十进制）</param>
        /// <returns>国标码</returns>
        static internal int Calc_GBbyQW(int qw)
        {
            string qw_high = Convert.ToString(qw / 100, 16);
            string qw_low = Convert.ToString(qw % 100, 16);
            Debug.WriteLine("qw_high: 0x" + qw_high);
            Debug.WriteLine("qw_low: 0x" + qw_low);

            int gb_high = Convert.ToInt32(qw_high, 16) + 0x20;
            int gb_low = Convert.ToInt32(qw_low, 16) + 0x20;
            Debug.WriteLine("gb_high: 0x" + Convert.ToString(gb_high, 16));
            Debug.WriteLine("gb_low: 0x" + Convert.ToString(gb_low, 16));

            string gb_str = Convert.ToString(gb_high, 16) + Convert.ToString(gb_low, 16);
            Debug.WriteLine("gb_str: 0x" + gb_str);

            return Convert.ToInt32(gb_str, 16);
        }


        /// <summary>
        /// 由区位码计算国标码
        /// </summary>
        /// <param name="qw">区位码（十进制）</param>
        /// <returns>国标码</returns>
        static internal int Calc_JNbyQW(int qw)
        {
            string qw_high = Convert.ToString(qw / 100, 16);
            string qw_low = Convert.ToString(qw % 100, 16);
            Debug.WriteLine("qw_high: 0x" + qw_high);
            Debug.WriteLine("qw_low: 0x" + qw_low);

            int gb_high = Convert.ToInt32(qw_high, 16) + 0xA0;
            int gb_low = Convert.ToInt32(qw_low, 16) + 0xA0;
            Debug.WriteLine("gb_high: 0x" + Convert.ToString(gb_high, 16));
            Debug.WriteLine("gb_low: 0x" + Convert.ToString(gb_low, 16));

            string gb_str = Convert.ToString(gb_high, 16) + Convert.ToString(gb_low, 16);
            Debug.WriteLine("gb_str: 0x" + gb_str);

            return Convert.ToInt32(gb_str, 16);
        }


        /// <summary>
        /// 比较汉字（或字符串）大小
        /// </summary>
        /// <param name="chn1">汉字1</param>
        /// <param name="chn2">汉字2</param>
        /// <returns>1-字符1>字符2/0-字符1=字符2/-1-字符1<字符2</returns>
        static internal int Calc_compare(string chn1, string chn2)
        {
            // 1 ： str1大于str2
            // 0 ： str1等于str2
            // -1 ： str1小于str2

            return string.Compare(chn1, chn2);

        }

    }
}
