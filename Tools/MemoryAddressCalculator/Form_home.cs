﻿namespace MemoryAddressCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
            comboBox_byBus_resultUnit.SelectedIndex = 1;
            comboBox_byAddress_resultUnit.SelectedIndex = 1;
            comboBox_byAddressStart_capUnit.SelectedIndex = 1;
            comboBox_byAddressEnd_capUnit.SelectedIndex = 1;
        }


        private void button_byBus_calc_Click(object sender, EventArgs e)
        {
            // 获得结果（单位为Byte）
            double result = CMemoryAddress.Calc_byBus((int)numericUpDown_byBus_busNum.Value);

            // 转换为所需的单位
            decimal result_unit = CStorageUnit.Convert((decimal)result, "B", comboBox_byBus_resultUnit.Text);

            // 显示结果
            label_byBus_result.Text = result_unit.ToString();
        }


        private void button_byAdress_calc_Click(object sender, EventArgs e)
        {
            // 获得结果（单位为Byte）
            double result = CMemoryAddress.Calc_byAddress((int)numericUpDown_byAddress_addressStart.Value, (int)numericUpDown_byAddress_addressEnd.Value);

            // 转换为所需的单位
            decimal result_unit = CStorageUnit.Convert((decimal)result, "B", comboBox_byAddress_resultUnit.Text);

            // 显示结果
            label_byAddress_result.Text = result_unit.ToString();
        }



        private void button_byAddressStart_calc_Click(object sender, EventArgs e)
        {
            // 转换为所需单位的数值
            decimal cap_result = CStorageUnit.Convert((decimal)numericUpDown_byAddressStart_cap.Value, comboBox_byAddressStart_capUnit.Text,"B");

            // 获得结果（单位为Byte）
            double result = CMemoryAddress.Calc_byAddressStart((int)cap_result, (int)numericUpDown_byAddressStart_addressStart.Value);

            // 显示结果
            label1_byAddressStart_result.Text = Convert.ToString((int)result,16).ToUpper();
        }



        private void button_byAddressEnd_calc_Click(object sender, EventArgs e)
        {
            // 转换为所需单位的数值
            decimal cap_result = CStorageUnit.Convert((decimal)numericUpDown_byAddressEnd_cap.Value, comboBox_byAddressEnd_capUnit.Text, "B");

            // 获得结果（单位为Byte）
            double result = CMemoryAddress.Calc_byAddressEnd((int)cap_result, (int)numericUpDown_byAddressEnd_addressEnd.Value);

            // 显示结果
            label1_byAddressEnd_result.Text = Convert.ToString((int)result, 16).ToUpper();
        }
    }
}
