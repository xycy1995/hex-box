﻿namespace MemoryAddressCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button_byBus_calc = new System.Windows.Forms.Button();
            this.numericUpDown_byBus_busNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_byBus_resultUnit = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label_byBus_result = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button_byAdress_calc = new System.Windows.Forms.Button();
            this.numericUpDown_byAddress_addressEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_byAddress_addressStart = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_byAddress_resultUnit = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label_byAddress_result = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button_byAddressStart_calc = new System.Windows.Forms.Button();
            this.numericUpDown_byAddressStart_addressStart = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_byAddressStart_cap = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox_byAddressStart_capUnit = new System.Windows.Forms.ComboBox();
            this.label1_byAddressStart_result = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button_byAddressEnd_calc = new System.Windows.Forms.Button();
            this.numericUpDown_byAddressEnd_addressEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_byAddressEnd_cap = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox_byAddressEnd_capUnit = new System.Windows.Forms.ComboBox();
            this.label1_byAddressEnd_result = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byBus_busNum)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddress_addressEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddress_addressStart)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressStart_addressStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressStart_cap)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressEnd_addressEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressEnd_cap)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(19, 19);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(832, 606);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button_byBus_calc);
            this.tabPage1.Controls.Add(this.numericUpDown_byBus_busNum);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.comboBox_byBus_resultUnit);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label_byBus_result);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(8, 45);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage1.Size = new System.Drawing.Size(816, 553);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "已知地址线";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button_byBus_calc
            // 
            this.button_byBus_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_byBus_calc.Location = new System.Drawing.Point(9, 488);
            this.button_byBus_calc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_byBus_calc.Name = "button_byBus_calc";
            this.button_byBus_calc.Size = new System.Drawing.Size(801, 62);
            this.button_byBus_calc.TabIndex = 6;
            this.button_byBus_calc.Text = "计算";
            this.button_byBus_calc.UseVisualStyleBackColor = true;
            this.button_byBus_calc.Click += new System.EventHandler(this.button_byBus_calc_Click);
            // 
            // numericUpDown_byBus_busNum
            // 
            this.numericUpDown_byBus_busNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_byBus_busNum.Location = new System.Drawing.Point(149, 378);
            this.numericUpDown_byBus_busNum.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.numericUpDown_byBus_busNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_byBus_busNum.Name = "numericUpDown_byBus_busNum";
            this.numericUpDown_byBus_busNum.Size = new System.Drawing.Size(661, 38);
            this.numericUpDown_byBus_busNum.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 381);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 31);
            this.label4.TabIndex = 4;
            this.label4.Text = "地址线数量";
            // 
            // comboBox_byBus_resultUnit
            // 
            this.comboBox_byBus_resultUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_byBus_resultUnit.FormattingEnabled = true;
            this.comboBox_byBus_resultUnit.Items.AddRange(new object[] {
            "b",
            "B",
            "KB",
            "MB",
            "GB",
            "TB"});
            this.comboBox_byBus_resultUnit.Location = new System.Drawing.Point(661, 294);
            this.comboBox_byBus_resultUnit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.comboBox_byBus_resultUnit.Name = "comboBox_byBus_resultUnit";
            this.comboBox_byBus_resultUnit.Size = new System.Drawing.Size(147, 39);
            this.comboBox_byBus_resultUnit.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(544, 299);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "目标单位";
            // 
            // label_byBus_result
            // 
            this.label_byBus_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_byBus_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_byBus_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_byBus_result.Location = new System.Drawing.Point(9, 53);
            this.label_byBus_result.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label_byBus_result.Name = "label_byBus_result";
            this.label_byBus_result.Size = new System.Drawing.Size(800, 236);
            this.label_byBus_result.TabIndex = 1;
            this.label_byBus_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "计算结果";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button_byAdress_calc);
            this.tabPage2.Controls.Add(this.numericUpDown_byAddress_addressEnd);
            this.tabPage2.Controls.Add(this.numericUpDown_byAddress_addressStart);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.comboBox_byAddress_resultUnit);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label_byAddress_result);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Location = new System.Drawing.Point(8, 45);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage2.Size = new System.Drawing.Size(816, 553);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "已知首末地址";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button_byAdress_calc
            // 
            this.button_byAdress_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_byAdress_calc.Location = new System.Drawing.Point(9, 485);
            this.button_byAdress_calc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_byAdress_calc.Name = "button_byAdress_calc";
            this.button_byAdress_calc.Size = new System.Drawing.Size(801, 62);
            this.button_byAdress_calc.TabIndex = 13;
            this.button_byAdress_calc.Text = "计算";
            this.button_byAdress_calc.UseVisualStyleBackColor = true;
            this.button_byAdress_calc.Click += new System.EventHandler(this.button_byAdress_calc_Click);
            // 
            // numericUpDown_byAddress_addressEnd
            // 
            this.numericUpDown_byAddress_addressEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_byAddress_addressEnd.Hexadecimal = true;
            this.numericUpDown_byAddress_addressEnd.Location = new System.Drawing.Point(103, 418);
            this.numericUpDown_byAddress_addressEnd.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.numericUpDown_byAddress_addressEnd.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_byAddress_addressEnd.Name = "numericUpDown_byAddress_addressEnd";
            this.numericUpDown_byAddress_addressEnd.Size = new System.Drawing.Size(708, 38);
            this.numericUpDown_byAddress_addressEnd.TabIndex = 12;
            // 
            // numericUpDown_byAddress_addressStart
            // 
            this.numericUpDown_byAddress_addressStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_byAddress_addressStart.Hexadecimal = true;
            this.numericUpDown_byAddress_addressStart.Location = new System.Drawing.Point(103, 367);
            this.numericUpDown_byAddress_addressStart.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.numericUpDown_byAddress_addressStart.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_byAddress_addressStart.Name = "numericUpDown_byAddress_addressStart";
            this.numericUpDown_byAddress_addressStart.Size = new System.Drawing.Size(708, 38);
            this.numericUpDown_byAddress_addressStart.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 422);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 31);
            this.label9.TabIndex = 11;
            this.label9.Text = "末地址";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 370);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 31);
            this.label5.TabIndex = 11;
            this.label5.Text = "首地址";
            // 
            // comboBox_byAddress_resultUnit
            // 
            this.comboBox_byAddress_resultUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_byAddress_resultUnit.FormattingEnabled = true;
            this.comboBox_byAddress_resultUnit.Items.AddRange(new object[] {
            "b",
            "B",
            "KB",
            "MB",
            "GB",
            "TB"});
            this.comboBox_byAddress_resultUnit.Location = new System.Drawing.Point(661, 293);
            this.comboBox_byAddress_resultUnit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.comboBox_byAddress_resultUnit.Name = "comboBox_byAddress_resultUnit";
            this.comboBox_byAddress_resultUnit.Size = new System.Drawing.Size(147, 39);
            this.comboBox_byAddress_resultUnit.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(544, 298);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 31);
            this.label6.TabIndex = 9;
            this.label6.Text = "目标单位";
            // 
            // label_byAddress_result
            // 
            this.label_byAddress_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_byAddress_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_byAddress_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_byAddress_result.Location = new System.Drawing.Point(9, 54);
            this.label_byAddress_result.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label_byAddress_result.Name = "label_byAddress_result";
            this.label_byAddress_result.Size = new System.Drawing.Size(800, 233);
            this.label_byAddress_result.TabIndex = 8;
            this.label_byAddress_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 6);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 31);
            this.label8.TabIndex = 7;
            this.label8.Text = "计算结果";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button_byAddressStart_calc);
            this.tabPage3.Controls.Add(this.numericUpDown_byAddressStart_addressStart);
            this.tabPage3.Controls.Add(this.numericUpDown_byAddressStart_cap);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.comboBox_byAddressStart_capUnit);
            this.tabPage3.Controls.Add(this.label1_byAddressStart_result);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Location = new System.Drawing.Point(8, 45);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage3.Size = new System.Drawing.Size(816, 553);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "已知容量和首地址";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button_byAddressStart_calc
            // 
            this.button_byAddressStart_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_byAddressStart_calc.Location = new System.Drawing.Point(9, 485);
            this.button_byAddressStart_calc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_byAddressStart_calc.Name = "button_byAddressStart_calc";
            this.button_byAddressStart_calc.Size = new System.Drawing.Size(801, 62);
            this.button_byAddressStart_calc.TabIndex = 22;
            this.button_byAddressStart_calc.Text = "计算";
            this.button_byAddressStart_calc.UseVisualStyleBackColor = true;
            this.button_byAddressStart_calc.Click += new System.EventHandler(this.button_byAddressStart_calc_Click);
            // 
            // numericUpDown_byAddressStart_addressStart
            // 
            this.numericUpDown_byAddressStart_addressStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_byAddressStart_addressStart.Hexadecimal = true;
            this.numericUpDown_byAddressStart_addressStart.Location = new System.Drawing.Point(126, 420);
            this.numericUpDown_byAddressStart_addressStart.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.numericUpDown_byAddressStart_addressStart.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_byAddressStart_addressStart.Name = "numericUpDown_byAddressStart_addressStart";
            this.numericUpDown_byAddressStart_addressStart.Size = new System.Drawing.Size(684, 38);
            this.numericUpDown_byAddressStart_addressStart.TabIndex = 20;
            // 
            // numericUpDown_byAddressStart_cap
            // 
            this.numericUpDown_byAddressStart_cap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_byAddressStart_cap.Location = new System.Drawing.Point(126, 369);
            this.numericUpDown_byAddressStart_cap.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.numericUpDown_byAddressStart_cap.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_byAddressStart_cap.Name = "numericUpDown_byAddressStart_cap";
            this.numericUpDown_byAddressStart_cap.Size = new System.Drawing.Size(526, 38);
            this.numericUpDown_byAddressStart_cap.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 423);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 31);
            this.label10.TabIndex = 18;
            this.label10.Text = "首地址";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 372);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 31);
            this.label11.TabIndex = 19;
            this.label11.Text = "内存容量";
            // 
            // comboBox_byAddressStart_capUnit
            // 
            this.comboBox_byAddressStart_capUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_byAddressStart_capUnit.FormattingEnabled = true;
            this.comboBox_byAddressStart_capUnit.Items.AddRange(new object[] {
            "b",
            "B",
            "KB",
            "MB",
            "GB",
            "TB"});
            this.comboBox_byAddressStart_capUnit.Location = new System.Drawing.Point(661, 369);
            this.comboBox_byAddressStart_capUnit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.comboBox_byAddressStart_capUnit.Name = "comboBox_byAddressStart_capUnit";
            this.comboBox_byAddressStart_capUnit.Size = new System.Drawing.Size(147, 39);
            this.comboBox_byAddressStart_capUnit.TabIndex = 17;
            // 
            // label1_byAddressStart_result
            // 
            this.label1_byAddressStart_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1_byAddressStart_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1_byAddressStart_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1_byAddressStart_result.Location = new System.Drawing.Point(9, 56);
            this.label1_byAddressStart_result.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1_byAddressStart_result.Name = "label1_byAddressStart_result";
            this.label1_byAddressStart_result.Size = new System.Drawing.Size(800, 233);
            this.label1_byAddressStart_result.TabIndex = 15;
            this.label1_byAddressStart_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 8);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 31);
            this.label14.TabIndex = 14;
            this.label14.Text = "末地址";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button_byAddressEnd_calc);
            this.tabPage4.Controls.Add(this.numericUpDown_byAddressEnd_addressEnd);
            this.tabPage4.Controls.Add(this.numericUpDown_byAddressEnd_cap);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.comboBox_byAddressEnd_capUnit);
            this.tabPage4.Controls.Add(this.label1_byAddressEnd_result);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Location = new System.Drawing.Point(8, 45);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage4.Size = new System.Drawing.Size(816, 553);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "已知容量和末地址";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button_byAddressEnd_calc
            // 
            this.button_byAddressEnd_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_byAddressEnd_calc.Location = new System.Drawing.Point(9, 485);
            this.button_byAddressEnd_calc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_byAddressEnd_calc.Name = "button_byAddressEnd_calc";
            this.button_byAddressEnd_calc.Size = new System.Drawing.Size(801, 62);
            this.button_byAddressEnd_calc.TabIndex = 30;
            this.button_byAddressEnd_calc.Text = "计算";
            this.button_byAddressEnd_calc.UseVisualStyleBackColor = true;
            this.button_byAddressEnd_calc.Click += new System.EventHandler(this.button_byAddressEnd_calc_Click);
            // 
            // numericUpDown_byAddressEnd_addressEnd
            // 
            this.numericUpDown_byAddressEnd_addressEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_byAddressEnd_addressEnd.Hexadecimal = true;
            this.numericUpDown_byAddressEnd_addressEnd.Location = new System.Drawing.Point(126, 420);
            this.numericUpDown_byAddressEnd_addressEnd.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.numericUpDown_byAddressEnd_addressEnd.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_byAddressEnd_addressEnd.Name = "numericUpDown_byAddressEnd_addressEnd";
            this.numericUpDown_byAddressEnd_addressEnd.Size = new System.Drawing.Size(684, 38);
            this.numericUpDown_byAddressEnd_addressEnd.TabIndex = 28;
            // 
            // numericUpDown_byAddressEnd_cap
            // 
            this.numericUpDown_byAddressEnd_cap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_byAddressEnd_cap.Location = new System.Drawing.Point(126, 369);
            this.numericUpDown_byAddressEnd_cap.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.numericUpDown_byAddressEnd_cap.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_byAddressEnd_cap.Name = "numericUpDown_byAddressEnd_cap";
            this.numericUpDown_byAddressEnd_cap.Size = new System.Drawing.Size(526, 38);
            this.numericUpDown_byAddressEnd_cap.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 423);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 31);
            this.label12.TabIndex = 26;
            this.label12.Text = "末地址";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 372);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 31);
            this.label15.TabIndex = 27;
            this.label15.Text = "内存容量";
            // 
            // comboBox_byAddressEnd_capUnit
            // 
            this.comboBox_byAddressEnd_capUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_byAddressEnd_capUnit.FormattingEnabled = true;
            this.comboBox_byAddressEnd_capUnit.Items.AddRange(new object[] {
            "b",
            "B",
            "KB",
            "MB",
            "GB",
            "TB"});
            this.comboBox_byAddressEnd_capUnit.Location = new System.Drawing.Point(661, 369);
            this.comboBox_byAddressEnd_capUnit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.comboBox_byAddressEnd_capUnit.Name = "comboBox_byAddressEnd_capUnit";
            this.comboBox_byAddressEnd_capUnit.Size = new System.Drawing.Size(147, 39);
            this.comboBox_byAddressEnd_capUnit.TabIndex = 25;
            // 
            // label1_byAddressEnd_result
            // 
            this.label1_byAddressEnd_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1_byAddressEnd_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1_byAddressEnd_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1_byAddressEnd_result.Location = new System.Drawing.Point(9, 56);
            this.label1_byAddressEnd_result.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1_byAddressEnd_result.Name = "label1_byAddressEnd_result";
            this.label1_byAddressEnd_result.Size = new System.Drawing.Size(800, 233);
            this.label1_byAddressEnd_result.TabIndex = 24;
            this.label1_byAddressEnd_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 8);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 31);
            this.label17.TabIndex = 23;
            this.label17.Text = "首地址";
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 643);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "Form_home";
            this.Text = "内存地址计算";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byBus_busNum)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddress_addressEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddress_addressStart)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressStart_addressStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressStart_cap)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressEnd_addressEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_byAddressEnd_cap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private Label label3;
        private Label label_byBus_result;
        private Label label1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private Button button_byBus_calc;
        private NumericUpDown numericUpDown_byBus_busNum;
        private Label label4;
        private ComboBox comboBox_byBus_resultUnit;
        private Button button_byAdress_calc;
        private NumericUpDown numericUpDown_byAddress_addressEnd;
        private NumericUpDown numericUpDown_byAddress_addressStart;
        private Label label9;
        private Label label5;
        private ComboBox comboBox_byAddress_resultUnit;
        private Label label6;
        private Label label_byAddress_result;
        private Label label8;
        private Button button_byAddressStart_calc;
        private NumericUpDown numericUpDown_byAddressStart_addressStart;
        private NumericUpDown numericUpDown_byAddressStart_cap;
        private Label label10;
        private Label label11;
        private ComboBox comboBox_byAddressStart_capUnit;
        private Label label1_byAddressStart_result;
        private Label label14;
        private Button button_byAddressEnd_calc;
        private NumericUpDown numericUpDown_byAddressEnd_addressEnd;
        private NumericUpDown numericUpDown_byAddressEnd_cap;
        private Label label12;
        private Label label15;
        private ComboBox comboBox_byAddressEnd_capUnit;
        private Label label1_byAddressEnd_result;
        private Label label17;
    }
}