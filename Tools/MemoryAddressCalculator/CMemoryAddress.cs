﻿namespace MemoryAddressCalculator
{
    internal class CMemoryAddress
    {
        /// <summary>
        /// 根据地址线的数量，求容量
        /// </summary>
        /// <param name="busNum">地址线的数量</param>
        /// <returns>容量，单位为Byte</returns>
        static internal double Calc_byBus(int busNum)
        {
            if (busNum == 0)
            {
                return 0;
            }
            else
            {
                return Math.Pow(2, busNum);
            }
        }


        /// <summary>
        /// 根据首末地址，求容量
        /// </summary>
        /// <param name="addressStart">首地址</param>
        /// <param name="addressEnd">末地址</param>
        /// <returns>容量，单位为Byte</returns>
        static internal int Calc_byAddress(int addressStart, int addressEnd)
        {
            // 如果首地址=末地址
            if (addressStart == addressEnd)
            {
                return 0;
            }
            // 如果首地址小于末地址
            else if (addressStart < addressEnd)
            {
                return addressEnd - addressStart + 1;
            }
            // 如果首地址大于末地址
            else
            {
                return -1;  // 返回-1表示出错
            }
        }


        /// <summary>
        /// 已知内存容量和首地址，求得末地址
        /// </summary>
        /// <param name="capNum">内存容量（Byte）</param>
        /// <param name="addressStart">首地址</param>
        /// <returns>末地址</returns>
        static internal int Calc_byAddressStart(int capNum, int addressStart)
        {
            if (capNum == 0)
            {
                return addressStart;
            }
            else
            {
                return capNum + addressStart - 1;
            }

        }


        /// <summary>
        /// 已知内存容量和末地址，求得首地址
        /// </summary>
        /// <param name="capNum">内存容量（Byte）</param>
        /// <param name="addressEnd">末地址</param>
        /// <returns>首地址</returns>
        static internal int Calc_byAddressEnd(int capNum, int addressEnd)
        {
            if (capNum == 0)
            {
                return addressEnd;
            }
            else
            {
                return addressEnd - capNum + 1;
            }

        }
    }
}
