﻿using HexBoxPluginBase;

namespace RadixCalculator
{
    public class CMain : ITool
    {
        public string Name => "进制计算器";

        public string Version => "1.0.1";
        public string Author => "斜影重阳xycy";

        public string Describe => "二进制、八进制、十进制、十六进制之间的计算";

        public Icon Icon => Properties.Resources.AppIcon;

        public object CreateFormInstance()
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }

    }
}
