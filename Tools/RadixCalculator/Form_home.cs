﻿using System.Diagnostics;

namespace RadixCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
        }

       

        private void button_bin_Click(object sender, EventArgs e)
        {
            // 还未做格式限制

            textBox_oct.Text = CRadix.Convert(textBox_bin.Text, "BIN", "OCT");
            textBox_dec.Text = CRadix.Convert(textBox_bin.Text, "BIN", "DEC");
            textBox_hex.Text = CRadix.Convert(textBox_bin.Text, "BIN", "HEX");

        }

        private void button_oct_Click(object sender, EventArgs e)
        {
            // 还未做格式限制

            textBox_bin.Text = CRadix.Convert(textBox_oct.Text, "OCT", "BIN");
            textBox_dec.Text = CRadix.Convert(textBox_oct.Text, "OCT", "DEC");
            textBox_hex.Text = CRadix.Convert(textBox_oct.Text, "OCT", "HEX");

        }

        private void button_dec_Click(object sender, EventArgs e)
        {
            // 还未做格式限制

            textBox_bin.Text = CRadix.Convert(textBox_dec.Text, "DEC", "BIN");
            textBox_oct.Text = CRadix.Convert(textBox_dec.Text, "DEC", "OCT");
            textBox_hex.Text = CRadix.Convert(textBox_dec.Text, "DEC", "HEX");

        }

        private void button_hex_Click(object sender, EventArgs e)
        {
            // 还未做格式限制

            textBox_bin.Text = CRadix.Convert(textBox_hex.Text, "HEX", "BIN");
            textBox_oct.Text = CRadix.Convert(textBox_hex.Text, "HEX", "OCT");
            textBox_dec.Text = CRadix.Convert(textBox_hex.Text, "HEX", "DEC");

        }
    }
}
