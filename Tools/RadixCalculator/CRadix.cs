﻿using System.Diagnostics;

namespace RadixCalculator
{
    internal class CRadix
    {
        static internal string Convert(string source_num, string source_type, string target_radix)
        {
            // 如果源数据为 BIN
            if (source_type == "BIN")
            {
                if (target_radix == "BIN")// 如果目标进制为 BIN(输入与输出的进制一样)
                {
                    return source_num;
                }
                else if (target_radix == "OCT")
                {
                    // 先转为 DEC，再转为 OCT
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num, 2), 8);
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "DEC")
                {
                    // 直接转为 DEC
                    string result = System.Convert.ToInt32(source_num, 2).ToString();
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "HEX")
                {
                    // 先转为 DEC，再转为 HEX（大写形式）
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num, 2), 16).ToUpper();
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
            }


            // 如果源数据为 OCT
            else if (source_type == "OCT")
            {
                if (target_radix == "BIN")// 如果目标进制为 BIN
                {
                    // 先转为 DEC，再转为 BIN
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num, 8), 2);
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "OCT") // 如果和源进制一样
                {
                    return source_num;
                }
                else if (target_radix == "DEC")
                {
                    // 直接转为 DEC
                    string result = System.Convert.ToInt32(source_num, 8).ToString();
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "HEX")
                {
                    // 先转为 DEC，再转为 HEX（大写形式）
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num, 8), 16).ToUpper();
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
            }


            // 如果源数据为 DEC
            else if (source_type == "DEC")
            {
                if (target_radix == "BIN")// 如果目标进制为 BIN
                {
                    // 先转为 DEC（int类型），再转为 BIN
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num), 2);
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "OCT")
                {
                    // 先转为 DEC（int类型），再转为 OCT
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num), 8);
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "DEC") // 如果和源进制一样
                {
                    return source_num;
                }
                else if (target_radix == "HEX")
                {
                    // 先转为 DEC（int类型），再转为 HEX（大写形式）
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num), 16).ToUpper();
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
            }


            // 如果源数据为 HEX
            else if (source_type == "HEX")
            {
                if (target_radix == "BIN")// 如果目标进制为 BIN
                {
                    // 先转为 DEC（int类型），再转为 BIN
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num, 16), 2);
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "OCT")
                {
                    // 先转为 DEC（int类型），再转为 OCT
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num, 16), 8);
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "DEC")
                {
                    // 先转为 DEC（int 类型），再转为 DEC（string 类型）
                    string result = System.Convert.ToString(System.Convert.ToInt32(source_num, 16), 10);
                    Debug.WriteLine("转换结果：" + result);
                    return result;
                }
                else if (target_radix == "HEX")// 如果和源进制一样
                {
                    return source_num;
                }

            }

            // 否则直接返回"UNSUPPORT"
            return "UNSUPPORT";

        }
    }
}
