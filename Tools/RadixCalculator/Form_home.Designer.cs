﻿namespace RadixCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_bin = new System.Windows.Forms.TextBox();
            this.button_bin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_oct = new System.Windows.Forms.TextBox();
            this.button_oct = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_dec = new System.Windows.Forms.TextBox();
            this.button_dec = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_hex = new System.Windows.Forms.TextBox();
            this.button_hex = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "二进制（BIN）";
            // 
            // textBox_bin
            // 
            this.textBox_bin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_bin.Location = new System.Drawing.Point(147, 6);
            this.textBox_bin.Name = "textBox_bin";
            this.textBox_bin.Size = new System.Drawing.Size(218, 27);
            this.textBox_bin.TabIndex = 1;
            // 
            // button_bin
            // 
            this.button_bin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_bin.Location = new System.Drawing.Point(371, 4);
            this.button_bin.Name = "button_bin";
            this.button_bin.Size = new System.Drawing.Size(94, 29);
            this.button_bin.TabIndex = 2;
            this.button_bin.Text = "转换";
            this.button_bin.UseVisualStyleBackColor = true;
            this.button_bin.Click += new System.EventHandler(this.button_bin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "八进制（OCT）";
            // 
            // textBox_oct
            // 
            this.textBox_oct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_oct.Location = new System.Drawing.Point(147, 43);
            this.textBox_oct.Name = "textBox_oct";
            this.textBox_oct.Size = new System.Drawing.Size(218, 27);
            this.textBox_oct.TabIndex = 1;
            // 
            // button_oct
            // 
            this.button_oct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_oct.Location = new System.Drawing.Point(371, 41);
            this.button_oct.Name = "button_oct";
            this.button_oct.Size = new System.Drawing.Size(94, 29);
            this.button_oct.TabIndex = 2;
            this.button_oct.Text = "转换";
            this.button_oct.UseVisualStyleBackColor = true;
            this.button_oct.Click += new System.EventHandler(this.button_oct_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "十进制（DEC）";
            // 
            // textBox_dec
            // 
            this.textBox_dec.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_dec.Location = new System.Drawing.Point(147, 80);
            this.textBox_dec.Name = "textBox_dec";
            this.textBox_dec.Size = new System.Drawing.Size(218, 27);
            this.textBox_dec.TabIndex = 1;
            // 
            // button_dec
            // 
            this.button_dec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_dec.Location = new System.Drawing.Point(371, 78);
            this.button_dec.Name = "button_dec";
            this.button_dec.Size = new System.Drawing.Size(94, 29);
            this.button_dec.TabIndex = 2;
            this.button_dec.Text = "转换";
            this.button_dec.UseVisualStyleBackColor = true;
            this.button_dec.Click += new System.EventHandler(this.button_dec_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "十六进制（HEX）";
            // 
            // textBox_hex
            // 
            this.textBox_hex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_hex.Location = new System.Drawing.Point(147, 117);
            this.textBox_hex.Name = "textBox_hex";
            this.textBox_hex.Size = new System.Drawing.Size(218, 27);
            this.textBox_hex.TabIndex = 1;
            // 
            // button_hex
            // 
            this.button_hex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_hex.Location = new System.Drawing.Point(371, 115);
            this.button_hex.Name = "button_hex";
            this.button_hex.Size = new System.Drawing.Size(94, 29);
            this.button_hex.TabIndex = 2;
            this.button_hex.Text = "转换";
            this.button_hex.UseVisualStyleBackColor = true;
            this.button_hex.Click += new System.EventHandler(this.button_hex_Click);
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 152);
            this.Controls.Add(this.button_hex);
            this.Controls.Add(this.button_dec);
            this.Controls.Add(this.button_oct);
            this.Controls.Add(this.button_bin);
            this.Controls.Add(this.textBox_hex);
            this.Controls.Add(this.textBox_dec);
            this.Controls.Add(this.textBox_oct);
            this.Controls.Add(this.textBox_bin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form_home";
            this.Text = "进制转换";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox textBox_bin;
        private Button button_bin;
        private Label label2;
        private TextBox textBox_oct;
        private Button button_oct;
        private Label label3;
        private TextBox textBox_dec;
        private Button button_dec;
        private Label label4;
        private TextBox textBox_hex;
        private Button button_hex;
    }
}