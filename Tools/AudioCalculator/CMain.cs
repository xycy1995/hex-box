﻿using HexBoxPluginBase;

namespace AudioCalculator
{
    public class CMain : ITool
    {
        public string Name => "音频容量计算器";

        public string Version => "1.0.1";
        public string Author => "斜影重阳xycy";

        public string Describe => "计算音频文件容量大小";

        public Icon Icon => Properties.Resources.AppIcon;

        public object CreateFormInstance()
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }

    }
}
