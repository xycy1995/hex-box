﻿namespace AudioCalculator
{
    public partial class Form_home : System.Windows.Forms.Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
            comboBox_unit.SelectedIndex = 3;
        }


        private void button_calc_Click(object sender, EventArgs e)
        {
            decimal result = 0;

            result = CAudio.Calc(numericUpDown_hz.Value, numericUpDown_bit.Value, numericUpDown_channel.Value, numericUpDown_second.Value);

            decimal result_with_unit_change = CStorageUnit.Convert(result, "b", comboBox_unit.Text);

            label_result.Text = result_with_unit_change.ToString();

            //Debug.WriteLine(result);


        }

    }
}
