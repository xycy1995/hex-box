﻿namespace AudioCalculator
{
    internal class CAudio
    {

        /// <summary>
        /// 计算音频文件的大小（bit）    
        /// </summary>
        /// <param name="hz">声音频率（Hz）</param>
        /// <param name="bit">声音位数</param>
        /// <param name="channel">声道数</param>
        /// <param name="second">秒数</param>
        /// <returns>返回容量大小（bit）</returns>
        internal static decimal Calc(decimal hz, decimal bit, decimal channel, decimal second)
        {
            return hz * bit * channel * second;
        }
    }
}
