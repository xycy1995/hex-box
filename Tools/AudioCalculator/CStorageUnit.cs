﻿namespace AudioCalculator
{
    internal class CStorageUnit
    {
        internal static decimal Convert(decimal source_num, string source_unit, string target_unit)
        {
            // 设置数组，用于后面判断单位的大小
            string[] unit_arry = { "b", "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB", "NB", "DB" };

            int source_unit_id = unit_arry.ToList().IndexOf(source_unit);  // 获取源单位的索引值
            int target_unit_id = unit_arry.ToList().IndexOf(target_unit); ;  // 获取新单位的索引值
            int unit_diff = Math.Abs(target_unit_id - source_unit_id);  // 计算单位之间的级数差

            decimal target_unit_num = 0;

            // 如果 单位 小-->大
            if (source_unit_id < target_unit_id)
            {
                // 如果由 b 开始转换（要考虑b与B之间的8倍关系）
                if (source_unit == "b")
                {
                    if (target_unit == "B")
                    {
                        target_unit_num = source_num / 8;
                    }
                    else
                    {
                        // 将b-->B的过程单独提炼出来，然后进行剩下的计算 source/8/1024^n
                        target_unit_num = (decimal)((double)source_num / 8 / Math.Pow(1024, unit_diff - 1));
                    }

                }
                // 如果不是由 b 开始转换（无需考虑b与B之间的8倍关系）
                else
                {
                    target_unit_num = (decimal)((double)source_num / Math.Pow(1024, unit_diff));
                }

            }
            // 如果 单位 大-->小
            else if (source_unit_id > target_unit_id)
            {
                // 如果最终单位为b（要考虑b与B之间的8倍关系）
                if (target_unit == "b")
                {
                    if (source_unit == "B")
                    {
                        target_unit_num = source_num * 8;
                    }
                    else
                    {
                        target_unit_num = (decimal)((double)source_num * Math.Pow(1024, unit_diff - 1) * 8);
                    }
                }
                else
                {
                    // 无需考虑b与B之间的8倍关系
                    target_unit_num = (decimal)((double)source_num * Math.Pow(1024, unit_diff));
                }
            }

            // 如果一样（即不变）
            else
            {
                return source_num;  // 直接返回源数据
            }

            // 经过计算后得到最终的值，进行返回
            return target_unit_num;
        }
    }
}
