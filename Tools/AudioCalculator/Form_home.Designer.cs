﻿namespace AudioCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown_hz = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_bit = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_channel = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDown_second = new System.Windows.Forms.NumericUpDown();
            this.button_calc = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_unit = new System.Windows.Forms.ComboBox();
            this.label_result = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_channel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_second)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "频率（Hz）";
            // 
            // numericUpDown_hz
            // 
            this.numericUpDown_hz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_hz.Location = new System.Drawing.Point(106, 159);
            this.numericUpDown_hz.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_hz.Name = "numericUpDown_hz";
            this.numericUpDown_hz.Size = new System.Drawing.Size(175, 27);
            this.numericUpDown_hz.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "精度（bit）";
            // 
            // numericUpDown_bit
            // 
            this.numericUpDown_bit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_bit.Location = new System.Drawing.Point(105, 194);
            this.numericUpDown_bit.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_bit.Name = "numericUpDown_bit";
            this.numericUpDown_bit.Size = new System.Drawing.Size(176, 27);
            this.numericUpDown_bit.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "声道数";
            // 
            // numericUpDown_channel
            // 
            this.numericUpDown_channel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_channel.Location = new System.Drawing.Point(105, 227);
            this.numericUpDown_channel.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDown_channel.Name = "numericUpDown_channel";
            this.numericUpDown_channel.Size = new System.Drawing.Size(176, 27);
            this.numericUpDown_channel.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "时间（秒）";
            // 
            // numericUpDown_second
            // 
            this.numericUpDown_second.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_second.Location = new System.Drawing.Point(104, 262);
            this.numericUpDown_second.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_second.Name = "numericUpDown_second";
            this.numericUpDown_second.Size = new System.Drawing.Size(177, 27);
            this.numericUpDown_second.TabIndex = 5;
            // 
            // button_calc
            // 
            this.button_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_calc.Location = new System.Drawing.Point(10, 314);
            this.button_calc.Name = "button_calc";
            this.button_calc.Size = new System.Drawing.Size(270, 40);
            this.button_calc.TabIndex = 6;
            this.button_calc.Text = "计算";
            this.button_calc.UseVisualStyleBackColor = true;
            this.button_calc.Click += new System.EventHandler(this.button_calc_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(129, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "计算单位";
            // 
            // comboBox_unit
            // 
            this.comboBox_unit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_unit.FormattingEnabled = true;
            this.comboBox_unit.Items.AddRange(new object[] {
            "b",
            "B",
            "KB",
            "MB",
            "GB",
            "TB"});
            this.comboBox_unit.Location = new System.Drawing.Point(204, 116);
            this.comboBox_unit.Name = "comboBox_unit";
            this.comboBox_unit.Size = new System.Drawing.Size(78, 28);
            this.comboBox_unit.TabIndex = 1;
            // 
            // label_result
            // 
            this.label_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_result.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_result.Location = new System.Drawing.Point(10, 29);
            this.label_result.Name = "label_result";
            this.label_result.Size = new System.Drawing.Size(272, 84);
            this.label_result.TabIndex = 0;
            this.label_result.Text = "0";
            this.label_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "计算结果";
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 366);
            this.Controls.Add(this.label_result);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox_unit);
            this.Controls.Add(this.button_calc);
            this.Controls.Add(this.numericUpDown_second);
            this.Controls.Add(this.numericUpDown_channel);
            this.Controls.Add(this.numericUpDown_bit);
            this.Controls.Add(this.numericUpDown_hz);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(311, 413);
            this.Name = "Form_home";
            this.Text = "音频容量计算";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_bit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_channel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_second)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private NumericUpDown numericUpDown_hz;
        private Label label2;
        private NumericUpDown numericUpDown_bit;
        private Label label3;
        private NumericUpDown numericUpDown_channel;
        private Label label4;
        private NumericUpDown numericUpDown_second;
        private Button button_calc;
        private Label label5;
        private ComboBox comboBox_unit;
        private Label label_result;
        private Label label6;
    }
}