﻿using System.Diagnostics;

namespace AsciiCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
        }

        private void button_ascii_code_calc_Click(object sender, EventArgs e)
        {
            int result = CAscii.Convert(textBox_char.Text.ToCharArray()[0]);
            label_ascii_code_result.Text = result.ToString();
        }


        private void button_compare_calc_Click(object sender, EventArgs e)
        {
            int result = CAscii.Compare(textBox_char1.Text.ToCharArray()[0], textBox_char2.Text.ToCharArray()[0]);

            if (result == 1)
            {
                label_compare_result.Text = "字符1 > 字符2";
            }
            else if (result == 2)
            {
                label_compare_result.Text = "字符1 < 字符2";
            }
            else
            {
                label_compare_result.Text = "字符1 = 字符2";
            }
        }

    }
}
