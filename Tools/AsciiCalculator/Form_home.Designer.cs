﻿namespace AsciiCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button_ascii_code_calc = new System.Windows.Forms.Button();
            this.textBox_char = new System.Windows.Forms.TextBox();
            this.label_ascii_code_result = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button_compare_calc = new System.Windows.Forms.Button();
            this.textBox_char2 = new System.Windows.Forms.TextBox();
            this.textBox_char1 = new System.Windows.Forms.TextBox();
            this.label_compare_result = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(383, 316);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button_ascii_code_calc);
            this.tabPage1.Controls.Add(this.textBox_char);
            this.tabPage1.Controls.Add(this.label_ascii_code_result);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(375, 283);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ascii码值";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button_ascii_code_calc
            // 
            this.button_ascii_code_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ascii_code_calc.Location = new System.Drawing.Point(3, 237);
            this.button_ascii_code_calc.Name = "button_ascii_code_calc";
            this.button_ascii_code_calc.Size = new System.Drawing.Size(366, 40);
            this.button_ascii_code_calc.TabIndex = 2;
            this.button_ascii_code_calc.Text = "计算";
            this.button_ascii_code_calc.UseVisualStyleBackColor = true;
            this.button_ascii_code_calc.Click += new System.EventHandler(this.button_ascii_code_calc_Click);
            // 
            // textBox_char
            // 
            this.textBox_char.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_char.Location = new System.Drawing.Point(51, 159);
            this.textBox_char.MaxLength = 1;
            this.textBox_char.Name = "textBox_char";
            this.textBox_char.Size = new System.Drawing.Size(318, 27);
            this.textBox_char.TabIndex = 1;
            // 
            // label_ascii_code_result
            // 
            this.label_ascii_code_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ascii_code_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_ascii_code_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_ascii_code_result.Location = new System.Drawing.Point(3, 23);
            this.label_ascii_code_result.Name = "label_ascii_code_result";
            this.label_ascii_code_result.Size = new System.Drawing.Size(366, 125);
            this.label_ascii_code_result.TabIndex = 0;
            this.label_ascii_code_result.Text = "0";
            this.label_ascii_code_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "字符";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "计算结果";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button_compare_calc);
            this.tabPage2.Controls.Add(this.textBox_char2);
            this.tabPage2.Controls.Add(this.textBox_char1);
            this.tabPage2.Controls.Add(this.label_compare_result);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(375, 283);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "字符比较";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button_compare_calc
            // 
            this.button_compare_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_compare_calc.Location = new System.Drawing.Point(3, 237);
            this.button_compare_calc.Name = "button_compare_calc";
            this.button_compare_calc.Size = new System.Drawing.Size(366, 40);
            this.button_compare_calc.TabIndex = 3;
            this.button_compare_calc.Text = "计算";
            this.button_compare_calc.UseVisualStyleBackColor = true;
            this.button_compare_calc.Click += new System.EventHandler(this.button_compare_calc_Click);
            // 
            // textBox_char2
            // 
            this.textBox_char2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_char2.Location = new System.Drawing.Point(59, 196);
            this.textBox_char2.MaxLength = 1;
            this.textBox_char2.Name = "textBox_char2";
            this.textBox_char2.Size = new System.Drawing.Size(310, 27);
            this.textBox_char2.TabIndex = 2;
            // 
            // textBox_char1
            // 
            this.textBox_char1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_char1.Location = new System.Drawing.Point(59, 159);
            this.textBox_char1.MaxLength = 1;
            this.textBox_char1.Name = "textBox_char1";
            this.textBox_char1.Size = new System.Drawing.Size(310, 27);
            this.textBox_char1.TabIndex = 1;
            // 
            // label_compare_result
            // 
            this.label_compare_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_compare_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_compare_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_compare_result.Location = new System.Drawing.Point(3, 23);
            this.label_compare_result.Name = "label_compare_result";
            this.label_compare_result.Size = new System.Drawing.Size(366, 125);
            this.label_compare_result.TabIndex = 0;
            this.label_compare_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "计算结果";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "字符2";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "字符1";
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 340);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form_home";
            this.Text = "西文字符转换";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private Label label_ascii_code_result;
        private Label label3;
        private Label label1;
        private Button button_ascii_code_calc;
        private TextBox textBox_char;
        private Button button_compare_calc;
        private TextBox textBox_char2;
        private TextBox textBox_char1;
        private Label label_compare_result;
        private Label label6;
        private Label label7;
        private Label label5;
    }
}