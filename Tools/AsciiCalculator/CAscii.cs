﻿using System.Diagnostics;

namespace AsciiCalculator
{
    internal class CAscii
    {
        static internal int Convert(char character)
        {
            int result = System.Convert.ToInt32(character);
            Debug.WriteLine(result);

            return result;
        }

        static internal int Compare(char character1, char character2)
        {
            if (character1 > character2)
            {
                return 1;
            }
            else if (character1 < character2)
            {
                return 2;
            }
            else
            {
                return 0;
            }
        }
    }
}
