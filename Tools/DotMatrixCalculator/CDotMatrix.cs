﻿namespace DotMatrixCalculator
{
    internal class CDotMatrix
    {
        internal static decimal Calc(decimal row, decimal column, decimal count)
        {
            return row * column * count;
        }
    }
}
