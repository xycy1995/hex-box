﻿using HexBoxPluginBase;

namespace DotMatrixCalculator
{
    public class CMain : ITool
    {
        public string Name => "点阵容量计算器";

        public string Version => "1.0.1";
        public string Author => "斜影重阳xycy";

        public string Describe => "计算点阵容量大小";

        public Icon Icon => Properties.Resources.AppIcon;

        public object CreateFormInstance()
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }

    }
}
