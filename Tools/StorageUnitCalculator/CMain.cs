﻿using HexBoxPluginBase;

namespace StorageUnitCalculator
{
    public class CMain : ITool
    {
        public string Name => "存储单位计算器";

        public string Version => "1.0.1";
        public string Author => "斜影重阳xycy";

        public string Describe => "不同存储单位之间的计算";

        public Icon Icon => Properties.Resources.AppIcon;

        public object CreateFormInstance()
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }

    }
}
