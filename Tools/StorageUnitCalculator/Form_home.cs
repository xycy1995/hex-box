﻿using System.Diagnostics;

namespace StorageUnitCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
            comboBox_result.SelectedIndex = 0;
            comboBox_source.SelectedIndex = 0;

        }

        private void button_calc_Click(object sender, EventArgs e)
        {
            decimal result = CStorageUnit.Convert(numericUpDown_source.Value, comboBox_source.Text, comboBox_result.Text);

            label_result.Text = result.ToString();
        }

     

        
    }
}
