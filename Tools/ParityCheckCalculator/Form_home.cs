﻿using System.Diagnostics;

namespace ParityCheckCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
            comboBox_check_type.SelectedIndex = 0;
            comboBox_flag_type.SelectedIndex = 0;
        }


        private void button_check_calc_Click(object sender, EventArgs e)
        {
            string type;

            // 将选择的校验方式提取出来
            if (comboBox_check_type.Text == "奇校验")
            {
                type = "ODD";   // 奇校验
            }
            else
            {
                type = "EVEN";   // 偶校验
            }

            bool? result = CParityCheck.Check(textBox_check_number.Text, type);

            // 如果返回的不是 null
            if (result != null)
            {
                if (type == "ODD") // 如果选择的是奇校验
                {
                    if (result == true) // 如果返回为成功
                    {
                        label_check_result.Text = "【奇校验】验证通过";
                    }
                    else // 如果返回为失败
                    {
                        label_check_result.Text = "【奇校验】验证失败";
                    }

                }
                else if (type == "EVEN") // 如果选择的是偶校验
                {
                    if (result == true) // 如果返回为成功
                    {
                        label_check_result.Text = "【偶校验】验证通过";
                    }
                    else   // 如果返回为失败
                    {
                        label_check_result.Text = "【偶校验】验证失败";
                    }
                }

            }
            else
            {
                MessageBox.Show("请输入正确的数值", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_type_calc_Click(object sender, EventArgs e)
        {
            string? result = CParityCheck.Type(textBox_type_number.Text);
            if (result != null)
            {
                if (result == "EVEN")
                {
                    label_type_result.Text = "类型为【偶校验】";
                }
                else
                {
                    label_type_result.Text = "类型为【奇校验】";
                }
            }
            else
            {
                MessageBox.Show("请输入正确的数值", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_flag_calc_Click(object sender, EventArgs e)
        {
            string type;

            // 将选择的校验方式提取出来
            if (comboBox_check_type.Text == "奇校验")
            {
                type = "ODD";   // 奇校验
            }
            else
            {
                type = "EVEN";   // 偶校验
            }

            int? result = CParityCheck.Flag(textBox_flag_number.Text, type);

            // 如果返回的不是 null
            if (result != null)
            {
                label_flag_result.Text = "校验位为 " + result;
            }
            else
            {
                MessageBox.Show("请输入正确的数值","错误",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

        }

    }
}
