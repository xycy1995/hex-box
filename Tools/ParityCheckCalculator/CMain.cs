﻿using HexBoxPluginBase;

namespace ParityCheckCalculator
{
    public class CMain : ITool
    {
        public string Name => "校验码计算器";

        public string Version => "1.0.1";
        public string Author => "斜影重阳xycy";

        public string Describe => "奇偶校验的计算";

        public Icon Icon => Properties.Resources.AppIcon;

        public object CreateFormInstance()
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }

    }
}
