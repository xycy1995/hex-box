﻿namespace ParityCheckCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox_check_number = new System.Windows.Forms.TextBox();
            this.button_check_calc = new System.Windows.Forms.Button();
            this.comboBox_check_type = new System.Windows.Forms.ComboBox();
            this.label_check_result = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox_type_number = new System.Windows.Forms.TextBox();
            this.button_type_calc = new System.Windows.Forms.Button();
            this.label_type_result = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBox_flag_number = new System.Windows.Forms.TextBox();
            this.button_flag_calc = new System.Windows.Forms.Button();
            this.comboBox_flag_type = new System.Windows.Forms.ComboBox();
            this.label_flag_result = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(19, 19);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(646, 508);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox_check_number);
            this.tabPage1.Controls.Add(this.button_check_calc);
            this.tabPage1.Controls.Add(this.comboBox_check_type);
            this.tabPage1.Controls.Add(this.label_check_result);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(8, 45);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage1.Size = new System.Drawing.Size(630, 455);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "判断正误";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox_check_number
            // 
            this.textBox_check_number.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_check_number.Location = new System.Drawing.Point(184, 308);
            this.textBox_check_number.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_check_number.Name = "textBox_check_number";
            this.textBox_check_number.Size = new System.Drawing.Size(438, 38);
            this.textBox_check_number.TabIndex = 4;
            // 
            // button_check_calc
            // 
            this.button_check_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_check_calc.Location = new System.Drawing.Point(9, 387);
            this.button_check_calc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_check_calc.Name = "button_check_calc";
            this.button_check_calc.Size = new System.Drawing.Size(614, 62);
            this.button_check_calc.TabIndex = 3;
            this.button_check_calc.Text = "计算";
            this.button_check_calc.UseVisualStyleBackColor = true;
            this.button_check_calc.Click += new System.EventHandler(this.button_check_calc_Click);
            // 
            // comboBox_check_type
            // 
            this.comboBox_check_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_check_type.FormattingEnabled = true;
            this.comboBox_check_type.Items.AddRange(new object[] {
            "奇校验",
            "偶校验"});
            this.comboBox_check_type.Location = new System.Drawing.Point(184, 250);
            this.comboBox_check_type.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.comboBox_check_type.Name = "comboBox_check_type";
            this.comboBox_check_type.Size = new System.Drawing.Size(438, 39);
            this.comboBox_check_type.TabIndex = 1;
            // 
            // label_check_result
            // 
            this.label_check_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_check_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_check_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_check_result.Location = new System.Drawing.Point(9, 39);
            this.label_check_result.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label_check_result.Name = "label_check_result";
            this.label_check_result.Size = new System.Drawing.Size(613, 180);
            this.label_check_result.TabIndex = 0;
            this.label_check_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 313);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "数值（8位）";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 254);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 31);
            this.label3.TabIndex = 0;
            this.label3.Text = "校验码类型";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "计算结果";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox_type_number);
            this.tabPage2.Controls.Add(this.button_type_calc);
            this.tabPage2.Controls.Add(this.label_type_result);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Location = new System.Drawing.Point(8, 45);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage2.Size = new System.Drawing.Size(630, 455);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "判断类型";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox_type_number
            // 
            this.textBox_type_number.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_type_number.Location = new System.Drawing.Point(184, 308);
            this.textBox_type_number.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_type_number.Name = "textBox_type_number";
            this.textBox_type_number.Size = new System.Drawing.Size(438, 38);
            this.textBox_type_number.TabIndex = 9;
            // 
            // button_type_calc
            // 
            this.button_type_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_type_calc.Location = new System.Drawing.Point(9, 387);
            this.button_type_calc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_type_calc.Name = "button_type_calc";
            this.button_type_calc.Size = new System.Drawing.Size(614, 62);
            this.button_type_calc.TabIndex = 8;
            this.button_type_calc.Text = "计算";
            this.button_type_calc.UseVisualStyleBackColor = true;
            this.button_type_calc.Click += new System.EventHandler(this.button_type_calc_Click);
            // 
            // label_type_result
            // 
            this.label_type_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_type_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_type_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_type_result.Location = new System.Drawing.Point(9, 39);
            this.label_type_result.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label_type_result.Name = "label_type_result";
            this.label_type_result.Size = new System.Drawing.Size(613, 180);
            this.label_type_result.TabIndex = 4;
            this.label_type_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 313);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 31);
            this.label6.TabIndex = 5;
            this.label6.Text = "数值（8位）";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 31);
            this.label7.TabIndex = 6;
            this.label7.Text = "计算结果";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBox_flag_number);
            this.tabPage3.Controls.Add(this.button_flag_calc);
            this.tabPage3.Controls.Add(this.comboBox_flag_type);
            this.tabPage3.Controls.Add(this.label_flag_result);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Location = new System.Drawing.Point(8, 45);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tabPage3.Size = new System.Drawing.Size(630, 455);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "求校验位";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBox_flag_number
            // 
            this.textBox_flag_number.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_flag_number.Location = new System.Drawing.Point(184, 308);
            this.textBox_flag_number.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_flag_number.Name = "textBox_flag_number";
            this.textBox_flag_number.Size = new System.Drawing.Size(438, 38);
            this.textBox_flag_number.TabIndex = 11;
            // 
            // button_flag_calc
            // 
            this.button_flag_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_flag_calc.Location = new System.Drawing.Point(9, 387);
            this.button_flag_calc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_flag_calc.Name = "button_flag_calc";
            this.button_flag_calc.Size = new System.Drawing.Size(614, 62);
            this.button_flag_calc.TabIndex = 10;
            this.button_flag_calc.Text = "计算";
            this.button_flag_calc.UseVisualStyleBackColor = true;
            this.button_flag_calc.Click += new System.EventHandler(this.button_flag_calc_Click);
            // 
            // comboBox_flag_type
            // 
            this.comboBox_flag_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_flag_type.FormattingEnabled = true;
            this.comboBox_flag_type.Items.AddRange(new object[] {
            "奇校验",
            "偶校验"});
            this.comboBox_flag_type.Location = new System.Drawing.Point(184, 250);
            this.comboBox_flag_type.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.comboBox_flag_type.Name = "comboBox_flag_type";
            this.comboBox_flag_type.Size = new System.Drawing.Size(438, 39);
            this.comboBox_flag_type.TabIndex = 8;
            // 
            // label_flag_result
            // 
            this.label_flag_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_flag_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_flag_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_flag_result.Location = new System.Drawing.Point(9, 39);
            this.label_flag_result.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label_flag_result.Name = "label_flag_result";
            this.label_flag_result.Size = new System.Drawing.Size(613, 180);
            this.label_flag_result.TabIndex = 4;
            this.label_flag_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 313);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 31);
            this.label9.TabIndex = 5;
            this.label9.Text = "数值（7位）";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 254);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 31);
            this.label10.TabIndex = 6;
            this.label10.Text = "校验码类型";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 31);
            this.label11.TabIndex = 7;
            this.label11.Text = "计算结果";
            // 
            // Form_parityCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 546);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "Form_parityCheck";
            this.Text = "奇偶校验码计算";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private Label label_check_result;
        private Label label1;
        private TabPage tabPage3;
        private Button button_check_calc;
        private ComboBox comboBox_check_type;
        private Label label4;
        private Label label3;
        private Button button_type_calc;
        private Label label_type_result;
        private Label label6;
        private Label label7;
        private Button button_flag_calc;
        private ComboBox comboBox_flag_type;
        private Label label_flag_result;
        private Label label9;
        private Label label10;
        private Label label11;
        private TextBox textBox_check_number;
        private TextBox textBox_type_number;
        private TextBox textBox_flag_number;
    }
}