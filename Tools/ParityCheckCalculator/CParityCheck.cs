﻿namespace ParityCheckCalculator
{
    internal class CParityCheck
    {
        static internal bool? Check(string number, string type)
        {
            // 如果传入的数值为 8 位
            if (number.Length == 8)
            {
                int count = 0;  // 计数器

                // 把数值的1提取出来，用于计数
                int i = 0;
                while (i < number.Length)
                {
                    if (number[i] == '1')
                    {
                        count++;
                    }
                    i += 1;
                }

                if (type == "ODD") // 如果为奇校验
                {
                    if (count % 2 == 0) // 如果能除尽，说明是偶数个1
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (type == "EVEN") // 如果为偶校验
                {
                    if (count % 2 == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            // 如果前面的代码没能返回，在最终返回 null
            return null;
        }


        static internal string? Type(string number)
        {
            // 如果传入的数值为 8 位
            if (number.Length == 8)
            {
                int count = 0;  // 计数器

                // 把数值的1提取出来，用于计数
                int i = 0;
                while (i < number.Length)
                {
                    if (number[i] == '1')
                    {
                        count++;
                    }
                    i += 1;
                }

                if (count % 2 == 0)
                {
                    return "EVEN";
                }
                else
                {
                    return "ODD";
                }
            }


            // 如果前面的代码没能返回，在最终返回 null
            return null;
        }


        static internal int? Flag(string number, string type)
        {
            // 如果传入的数值为 7 位
            if (number.Length == 7)
            {
                int count = 0;  // 计数器

                // 把数值的1提取出来，用于计数
                int i = 0;
                while (i < number.Length)
                {
                    if (number[i] == '1')
                    {
                        count++;
                    }
                    i += 1;
                }

                if (type == "ODD") // 如果为奇校验
                {
                    if (count % 2 == 0) // 如果能除尽，说明是偶数个1
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (type == "EVEN") // 如果为偶校验
                {
                    if (count % 2 == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }

            // 如果前面的代码没能返回，在最终返回 null
            return null;
        }
    }
}
