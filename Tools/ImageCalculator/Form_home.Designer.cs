﻿namespace ImageCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_result = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_unit = new System.Windows.Forms.ComboBox();
            this.button_calc = new System.Windows.Forms.Button();
            this.numericUpDown_depth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_pxv = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_pxh = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_depth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_pxv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_pxh)).BeginInit();
            this.SuspendLayout();
            // 
            // label_result
            // 
            this.label_result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_result.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_result.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_result.Location = new System.Drawing.Point(10, 31);
            this.label_result.Name = "label_result";
            this.label_result.Size = new System.Drawing.Size(272, 77);
            this.label_result.TabIndex = 0;
            this.label_result.Text = "0";
            this.label_result.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "计算结果";
            // 
            // comboBox_unit
            // 
            this.comboBox_unit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_unit.FormattingEnabled = true;
            this.comboBox_unit.Items.AddRange(new object[] {
            "b",
            "B",
            "KB",
            "MB",
            "GB",
            "TB"});
            this.comboBox_unit.Location = new System.Drawing.Point(204, 111);
            this.comboBox_unit.Name = "comboBox_unit";
            this.comboBox_unit.Size = new System.Drawing.Size(78, 28);
            this.comboBox_unit.TabIndex = 1;
            // 
            // button_calc
            // 
            this.button_calc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_calc.Location = new System.Drawing.Point(12, 274);
            this.button_calc.Name = "button_calc";
            this.button_calc.Size = new System.Drawing.Size(270, 40);
            this.button_calc.TabIndex = 5;
            this.button_calc.Text = "计算";
            this.button_calc.UseVisualStyleBackColor = true;
            this.button_calc.Click += new System.EventHandler(this.button_calc_Click);
            // 
            // numericUpDown_depth
            // 
            this.numericUpDown_depth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_depth.Location = new System.Drawing.Point(106, 228);
            this.numericUpDown_depth.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_depth.Name = "numericUpDown_depth";
            this.numericUpDown_depth.Size = new System.Drawing.Size(176, 27);
            this.numericUpDown_depth.TabIndex = 4;
            // 
            // numericUpDown_pxv
            // 
            this.numericUpDown_pxv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_pxv.Location = new System.Drawing.Point(106, 195);
            this.numericUpDown_pxv.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_pxv.Name = "numericUpDown_pxv";
            this.numericUpDown_pxv.Size = new System.Drawing.Size(176, 27);
            this.numericUpDown_pxv.TabIndex = 3;
            // 
            // numericUpDown_pxh
            // 
            this.numericUpDown_pxh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_pxh.Location = new System.Drawing.Point(107, 160);
            this.numericUpDown_pxh.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_pxh.Name = "numericUpDown_pxh";
            this.numericUpDown_pxh.Size = new System.Drawing.Size(175, 27);
            this.numericUpDown_pxh.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(129, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "计算单位";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 231);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "颜色深度";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "垂直像素";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "水平像素";
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 326);
            this.Controls.Add(this.label_result);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox_unit);
            this.Controls.Add(this.button_calc);
            this.Controls.Add(this.numericUpDown_depth);
            this.Controls.Add(this.numericUpDown_pxv);
            this.Controls.Add(this.numericUpDown_pxh);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form_home";
            this.Text = "图像容量计算";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_depth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_pxv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_pxh)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label_result;
        private Label label6;
        private ComboBox comboBox_unit;
        private Button button_calc;
        private NumericUpDown numericUpDown_depth;
        private NumericUpDown numericUpDown_pxv;
        private NumericUpDown numericUpDown_pxh;
        private Label label5;
        private Label label3;
        private Label label2;
        private Label label1;
    }
}