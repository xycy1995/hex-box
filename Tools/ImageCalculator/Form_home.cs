﻿namespace ImageCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
            comboBox_unit.SelectedIndex = 2;
        }

        private void button_calc_Click(object sender, EventArgs e)
        {
            decimal result = CImage.Calc(numericUpDown_pxh.Value, numericUpDown_pxv.Value, numericUpDown_depth.Value);

            decimal result_with_unit_change = CStorageUnit.Convert(result, "b", comboBox_unit.Text);

            label_result.Text = result_with_unit_change.ToString();
        }

    }
}
