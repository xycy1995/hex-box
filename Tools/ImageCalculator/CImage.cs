﻿namespace ImageCalculator
{
    internal class CImage
    {
        internal static decimal Calc(decimal pix_h, decimal pix_v, decimal depth)
        {
            return pix_h * pix_v * depth;
        }
    }
}
