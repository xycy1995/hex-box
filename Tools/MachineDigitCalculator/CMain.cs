﻿using HexBoxPluginBase;

namespace MachineDigitCalculator
{
    public class CMain : ITool
    {
        public string Name => "机器数计算器";

        public string Version => "1.0.1";
        public string Author => "斜影重阳xycy";

        public string Describe => "真值数与机器数（原码、反码、补码）之间的计算";

        public Icon Icon => Properties.Resources.AppIcon;

        public object CreateFormInstance()
        {
            Form_home form = new();  // 实例化
            return form;             // 返回实例
        }

    }
}
