﻿namespace MachineDigitCalculator
{
    internal class CMachineDigit
    {
        /// <summary>
        /// 根据真值数计算对应的原码（只限1字节即8位）
        /// </summary>
        /// <param name="truth">真值数</param>
        /// <returns>返回原码</returns>
        static internal int Truth_to_OC(int truth)
        {
            int truth_abs = Math.Abs(truth);  // 获取真值数的绝对值

            // 如果是负数
            if (truth < 0)
            {
                return truth_abs | 0b10000000;  // 在第8位加上1
            }
            // 如果为0或正数，返回本身
            else
            {
                return truth;
            }

        }


        /// <summary>
        /// 根据原码计算真值数（只限1字节即8位）
        /// </summary>
        /// <param name="oc">原码</param>
        /// <returns>真值数</returns>
        static internal int OC_to_Truth(int oc)
        {
            int flag = oc & 0b10000000;  // 获取标志位

            // 如果最高位是1（负数）
            if (flag == 0b10000000)
            {
                int value = oc & 0b01111111;    // 获取数值
                return -value;
            }
            //如果是正数
            else
            {
                return oc;
            }
        }


        /// <summary>
        /// 根据原码计算反码（只限1字节即8位）
        /// </summary>
        /// <param name="oc">原码</param>
        /// <returns>返回对应的反码</returns>
        static internal int OC_to_IC(int oc)
        {
            int flag = oc & 0b10000000;  // 获取标志位

            // 如果最高位是1（负数）
            if (flag == 0b10000000)
            {
                return oc ^ 0b01111111;// 除标志位，其余取反
            }
            //如果是正数
            else
            {
                return oc;
            }
        }


        /// <summary>
        /// 根据反码计算原码（只限1字节即8位）
        /// </summary>
        /// <param name="cc">反码</param>
        /// <returns>返回对应的原码</returns>
        static internal int IC_to_OC(int cc)
        {
            // 过程和原码-->反码是一样的，所以套个壳一样用
            return OC_to_IC(cc);
        }


        /// <summary>
        /// 根据反码计算补码（只限1字节即8位）
        /// </summary>
        /// <param name="ic">反码</param>
        /// <returns>返回对应的补码</returns>
        static internal int IC_to_CC(int ic)
        {
            int flag = ic & 0b10000000;  // 获取标志位

            // 如果最高位是1（负数）
            if (flag == 0b10000000)
            {
                int value = ic & 0b01111111;    // 获取数值
                int value_plus = value + 1 & 0b1111111;   // 给低7位+1，并只取7位，高于7位的内容丢弃
                return value_plus | 0b10000000; // 给最第8位设为1
            }
            //如果是正数
            else
            {
                return ic;
            }
        }


        /// <summary>
        /// 根据补码计算反码（只限1字节即8位）
        /// </summary>
        /// <param name="cc">补码</param>
        /// <returns>返回对应的反码</returns>
        static internal int CC_to_IC(int cc)
        {
            int flag = cc & 0b10000000;  // 获取标志位

            // 如果最高位是1（负数）
            if (flag == 0b10000000)
            {
                int value = cc & 0b01111111;    // 获取数值
                int value_plus = value - 1; //
                return value_plus | 0b10000000; // 给最第8位设为1
            }
            // 如果是正数
            else
            {
                return cc;
            }
        }

    }
}
