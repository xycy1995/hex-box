﻿namespace MachineDigitCalculator
{
    partial class Form_home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_complement = new System.Windows.Forms.Button();
            this.button_inverse = new System.Windows.Forms.Button();
            this.button_original = new System.Windows.Forms.Button();
            this.button_truthNumber = new System.Windows.Forms.Button();
            this.textBox_complement = new System.Windows.Forms.TextBox();
            this.textBox_inverse = new System.Windows.Forms.TextBox();
            this.textBox_original = new System.Windows.Forms.TextBox();
            this.textBox_truthNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_complement
            // 
            this.button_complement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_complement.Location = new System.Drawing.Point(577, 181);
            this.button_complement.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_complement.Name = "button_complement";
            this.button_complement.Size = new System.Drawing.Size(146, 45);
            this.button_complement.TabIndex = 11;
            this.button_complement.Text = "转换";
            this.button_complement.UseVisualStyleBackColor = true;
            this.button_complement.Click += new System.EventHandler(this.button_complement_Click);
            // 
            // button_inverse
            // 
            this.button_inverse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_inverse.Location = new System.Drawing.Point(577, 124);
            this.button_inverse.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_inverse.Name = "button_inverse";
            this.button_inverse.Size = new System.Drawing.Size(146, 45);
            this.button_inverse.TabIndex = 12;
            this.button_inverse.Text = "转换";
            this.button_inverse.UseVisualStyleBackColor = true;
            this.button_inverse.Click += new System.EventHandler(this.button_inverse_Click);
            // 
            // button_original
            // 
            this.button_original.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_original.Location = new System.Drawing.Point(577, 67);
            this.button_original.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_original.Name = "button_original";
            this.button_original.Size = new System.Drawing.Size(146, 45);
            this.button_original.TabIndex = 13;
            this.button_original.Text = "转换";
            this.button_original.UseVisualStyleBackColor = true;
            this.button_original.Click += new System.EventHandler(this.button_original_Click);
            // 
            // button_truthNumber
            // 
            this.button_truthNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_truthNumber.Location = new System.Drawing.Point(577, 9);
            this.button_truthNumber.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button_truthNumber.Name = "button_truthNumber";
            this.button_truthNumber.Size = new System.Drawing.Size(146, 45);
            this.button_truthNumber.TabIndex = 14;
            this.button_truthNumber.Text = "转换";
            this.button_truthNumber.UseVisualStyleBackColor = true;
            this.button_truthNumber.Click += new System.EventHandler(this.button_truthNumber_Click);
            // 
            // textBox_complement
            // 
            this.textBox_complement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_complement.Location = new System.Drawing.Point(112, 184);
            this.textBox_complement.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_complement.Name = "textBox_complement";
            this.textBox_complement.Size = new System.Drawing.Size(454, 38);
            this.textBox_complement.TabIndex = 7;
            // 
            // textBox_inverse
            // 
            this.textBox_inverse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_inverse.Location = new System.Drawing.Point(112, 127);
            this.textBox_inverse.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_inverse.Name = "textBox_inverse";
            this.textBox_inverse.Size = new System.Drawing.Size(454, 38);
            this.textBox_inverse.TabIndex = 8;
            // 
            // textBox_original
            // 
            this.textBox_original.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_original.Location = new System.Drawing.Point(112, 70);
            this.textBox_original.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_original.Name = "textBox_original";
            this.textBox_original.Size = new System.Drawing.Size(454, 38);
            this.textBox_original.TabIndex = 9;
            // 
            // textBox_truthNumber
            // 
            this.textBox_truthNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_truthNumber.Location = new System.Drawing.Point(112, 12);
            this.textBox_truthNumber.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_truthNumber.Name = "textBox_truthNumber";
            this.textBox_truthNumber.Size = new System.Drawing.Size(454, 38);
            this.textBox_truthNumber.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 189);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 31);
            this.label4.TabIndex = 3;
            this.label4.Text = "补码";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 132);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "反码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 31);
            this.label2.TabIndex = 5;
            this.label2.Text = "原码";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 31);
            this.label1.TabIndex = 6;
            this.label1.Text = "真值数";
            // 
            // Form_home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 236);
            this.Controls.Add(this.button_complement);
            this.Controls.Add(this.button_inverse);
            this.Controls.Add(this.button_original);
            this.Controls.Add(this.button_truthNumber);
            this.Controls.Add(this.textBox_complement);
            this.Controls.Add(this.textBox_inverse);
            this.Controls.Add(this.textBox_original);
            this.Controls.Add(this.textBox_truthNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "Form_home";
            this.Text = "机器数转换";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button button_complement;
        private Button button_inverse;
        private Button button_original;
        private Button button_truthNumber;
        private TextBox textBox_complement;
        private TextBox textBox_inverse;
        private TextBox textBox_original;
        private TextBox textBox_truthNumber;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
    }
}