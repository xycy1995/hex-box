﻿using System.Diagnostics;

namespace MachineDigitCalculator
{
    public partial class Form_home : Form
    {
        public Form_home()
        {
            InitializeComponent();

            // 初始化显示内容
            this.Icon = Properties.Resources.AppIcon;
        }

        

        private void button_truthNumber_Click(object sender, EventArgs e)
        {
            int truth = Convert.ToInt32(textBox_truthNumber.Text);
            int result_oc = CMachineDigit.Truth_to_OC(truth);
            int result_ic = CMachineDigit.OC_to_IC(result_oc);
            int result_cc = CMachineDigit.IC_to_CC(result_ic);

            Debug.WriteLine("Truth: " + truth);
            Debug.WriteLine("OC: " + result_oc);
            Debug.WriteLine("IC: " + result_ic);
            Debug.WriteLine("CC: " + result_cc);

            textBox_original.Text = Convert.ToString(result_oc, 2);
            textBox_inverse.Text = Convert.ToString(result_ic, 2);
            textBox_complement.Text = Convert.ToString(result_cc, 2);

        }

        private void button_original_Click(object sender, EventArgs e)
        {
            int oc = Convert.ToInt32(textBox_original.Text, 2);
            int result_truth = CMachineDigit.OC_to_Truth(oc);
            int result_ic = CMachineDigit.OC_to_IC(oc);
            int result_cc = CMachineDigit.IC_to_CC(result_ic);

            Debug.WriteLine("Truth: " + result_truth);
            Debug.WriteLine("OC: " + oc);
            Debug.WriteLine("IC: " + result_ic);
            Debug.WriteLine("CC: " + result_cc);

            textBox_truthNumber.Text = Convert.ToString(result_truth, 10);
            textBox_inverse.Text = Convert.ToString(result_ic, 2);
            textBox_complement.Text = Convert.ToString(result_cc, 2);
        }

        private void button_inverse_Click(object sender, EventArgs e)
        {
            int ic = Convert.ToInt32(textBox_inverse.Text, 2);
            int result_oc = CMachineDigit.IC_to_OC(ic);
            int result_truth = CMachineDigit.OC_to_Truth(result_oc);
            int result_cc = CMachineDigit.IC_to_CC(ic);

            Debug.WriteLine("Truth: " + result_truth);
            Debug.WriteLine("IC: " + ic);
            Debug.WriteLine("OC: " + result_oc);
            Debug.WriteLine("CC: " + result_cc);

            textBox_truthNumber.Text = Convert.ToString(result_truth, 10);
            textBox_original.Text = Convert.ToString(result_oc, 2);
            textBox_complement.Text = Convert.ToString(result_cc, 2);
        }

        private void button_complement_Click(object sender, EventArgs e)
        {
            int cc = Convert.ToInt32(textBox_complement.Text, 2);
            int result_ic = CMachineDigit.CC_to_IC(cc);
            int result_oc = CMachineDigit.IC_to_OC(result_ic);
            int result_truth = CMachineDigit.OC_to_Truth(result_oc);

            Debug.WriteLine("Truth: " + result_truth);
            Debug.WriteLine("IC: " + result_ic);
            Debug.WriteLine("OC: " + result_oc);
            Debug.WriteLine("CC: " + cc);

            textBox_truthNumber.Text = Convert.ToString(result_truth, 10);
            textBox_original.Text = Convert.ToString(result_oc, 2);
            textBox_inverse.Text = Convert.ToString(result_ic, 2);
        }
    }
}
