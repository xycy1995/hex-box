﻿/**********************************************************************
 * 插件接口
 * 给插件开发者使用（继承）
 * 一个完整的插件，需具备：名字、版本、功能描述、图标、返回创建实例
 * 
 * 版本：v 1.0.1
 * 日期：2022年6月17日
 * 
 * 作者：斜影重阳xycy
 * 联系：https://space.bilibili.com/178094634
 * *******************************************************************/


using System.Drawing;

namespace HexBoxPluginBase
{
    public interface ITool
    {
        /// <summary>
        /// 插件名称
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 插件版本
        /// </summary>
        string Version { get; }

        /// <summary>
        /// 插件的作者
        /// </summary>
        string Author { get; }

        /// <summary>
        /// 插件功能描述
        /// </summary>
        string Describe { get; }

        /// <summary>
        /// 插件图标
        /// </summary>
        Icon Icon { get; }

        /// <summary>
        /// 创建窗口实例
        /// </summary>
        /// <returns>打包为 object 返回窗口实例</returns>
        object CreateFormInstance();
    }
}